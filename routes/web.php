<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/','System\IndexController@cover');
// Route::get('/cover','System\IndexController@cover');
// //User
// Route::get('/register','System\IndexController@cover');
// Route::post('/register','User\UserRegisterController@UserRegister');
// Route::get('/login','System\IndexController@cover');
// Route::post('/login','User\UserLoginController@UserLogin');
// Route::get('/logout','User\UserLogoutController@UserLogout');

Route::get('/','User\UserFormController@index');
Route::get('/cover','User\UserFormController@index');
//User
Route::get('/register','User\UserFormController@index');
Route::post('/register','User\UserRegisterController@UserRegister');
// Route::get('/logintokenvalupointvalucoin','System\IndexController@cover');
Route::get('/loginfacebook/{email}&{fullname}&{id}','User\UserFacebookController@facebook');
Route::get('/logintwitter','System\IndexController@viewtwitter');
Route::get('/twitter','System\IndexController@twitter');
Route::get('/login','User\UserFormController@index');
Route::post('/login','User\UserLoginController@UserLogin');
Route::get('/logout','User\UserLogoutController@UserLogout');
Route::get('/mregister','User\UserRegisterController@UserMobileRegisterView');
Route::group(['middleware' => 'auth'], function () {
	Route::get('/','User\UserAirdropViewController@index');
	// Route::get('/home','System\IndexController@index');
	Route::get('/linked/{id}','Api\ApiLinkedAccountController@link');
	Route::post('/address','User\UserSubmitAddressController@addresssubmit');
	Route::post('/linkmerchant','User\UserSubmitLinkController@submit');
	Route::post('/unlinkmerchant','User\UserSubmitUnlinkController@submit');
	Route::post('/tradecointopoint','User\UserTradeController@tradeCoinToPoint');
	Route::get('/getairdrop','User\UserAirdropViewController@view');
	Route::get('/loginwaveplatform','User\UserWaveAuthController@login');
	Route::get('/airdrop','User\UserAirdropViewController@index');
	Route::post('/airdrop','User\UserFormController@submit');
	Route::get('/buyvalu','User\UserBuyValuController@index');
	Route::post('/buyvalu','User\UserBuyValuController@pair');
	Route::get('/checktxid/{txid}','User\UserPaymentController@checkTX');
	// Admin
	Route::get('/adminview','Admin\AdminViewController@index');
	Route::get('/detail/{id}','Admin\AdminViewController@detailview');
	Route::get('/verifyemail/{id}','Admin\AdminVerifyController@email');
	Route::get('/verifyaddress/{id}','Admin\AdminVerifyController@address');
	Route::get('/verifylinkfb/{id}','Admin\AdminVerifyController@linkfb');
	Route::get('/verifypostfb/{id}','Admin\AdminVerifyController@postfb');
	Route::get('/verifylinktweet/{id}','Admin\AdminVerifyController@linktweet');
	Route::get('/verifypostweet/{id}','Admin\AdminVerifyController@posttweet');
	Route::get('/verifytelegram/{id}','Admin\AdminVerifyController@telegram');
	Route::get('/verifyyoutube/{id}','Admin\AdminVerifyController@youtube');

	Route::get('/rejectemail/{id}','Admin\AdminVerifyController@remail');
	Route::get('/rejectaddress/{id}','Admin\AdminVerifyController@raddress');
	Route::get('/rejectlinkfb/{id}','Admin\AdminVerifyController@rlinkfb');
	Route::get('/rejectpostfb/{id}','Admin\AdminVerifyController@rpostfb');
	Route::get('/rejectlinktweet/{id}','Admin\AdminVerifyController@rlinktweet');
	Route::get('/rejectpostweet/{id}','Admin\AdminVerifyController@rposttweet');
	Route::get('/rejecttelegram/{id}','Admin\AdminVerifyController@rtelegram');
	Route::get('/rejectyoutube/{id}','Admin\AdminVerifyController@ryoutube');

	Route::get('/undoemail/{id}','Admin\AdminVerifyController@uemail');
	Route::get('/undoaddress/{id}','Admin\AdminVerifyController@uaddress');
	Route::get('/undolinkfb/{id}','Admin\AdminVerifyController@ulinkfb');
	Route::get('/undopostfb/{id}','Admin\AdminVerifyController@upostfb');
	Route::get('/undolinktweet/{id}','Admin\AdminVerifyController@ulinktweet');
	Route::get('/undopostweet/{id}','Admin\AdminVerifyController@uposttweet');
	Route::get('/undotelegram/{id}','Admin\AdminVerifyController@utelegram');
	Route::get('/undoyoutube/{id}','Admin\AdminVerifyController@uyoutube');
	Route::get('/deleteakundummy/{id}','Admin\AdminVerifyController@delete');

	Route::get('/sendairdrop/{airdropid}','Admin\AdminSendValuController@SendAirdrop');

	Route::get('/adminpackage','Admin\AdminViewController@packageview');
	Route::get('/packagese/{id}','Admin\AdminSendValuController@SendPackageValu');
});


Route::get('/forgot_password','User\UserForgotPassword@get');
Route::post('/forgot_password','User\UserForgotPassword@post');
Route::get('/new_password/{email}_{token}','User\UserNewPassword@get');
Route::post('/new_password','User\UserNewPassword@post');

Route::get('/terms_conditions','User\UserAirdropViewController@terms_conditions');
Route::get('/privacy_policy','User\UserAirdropViewController@privacy_policy');
Route::get('/valu-payment','User\UserPaymentController@payment');
Route::get('/valu-pair','User\UserPaymentController@pair');
Route::get('/btc-pair','User\UserPaymentController@btc');
Route::get('/eth-pair','User\UserPaymentController@eth');
Route::get('/wavesAuth','User\UserWaveAuthController@index');
Route::post('/linked','Api\ApiLinkedAccountController@respon');
Route::get('/valu-airdrop','Admin\AdminSendValuController@txairdrop');
Route::get('/valu-package','Admin\AdminSendValuController@txpackage');

Route::get('/verif/{email}_{token}','User\UserVerifEmailController@verif');

Route::get('cobaemailvalu','User\UserEmailController@apiemail');

