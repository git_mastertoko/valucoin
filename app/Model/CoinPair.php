<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoinPair extends Model
{
    use SoftDeletes;
    protected $table = "msvalupair";
    protected $primaryKey = "pair_id";

}