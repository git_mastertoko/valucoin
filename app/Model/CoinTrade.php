<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoinTrade extends Model
{
    use SoftDeletes;
    protected $table = "mstrade";
    protected $primaryKey = "trade_id";

}