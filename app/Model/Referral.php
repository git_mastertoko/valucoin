<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Referral extends Model
{
    use SoftDeletes;
    protected $table = "referral_bonus";
    protected $primaryKey = "referral_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }
}