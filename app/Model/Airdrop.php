<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Airdrop extends Model
{
    use SoftDeletes;
    protected $table = "msairdrop";
    protected $primaryKey = "airdrop_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }
}