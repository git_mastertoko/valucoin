<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuyValucoinPair extends Model
{
    use SoftDeletes;
    protected $table = "trs_buy_valucoin";
    protected $primaryKey = "buy_valucoin_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }
}