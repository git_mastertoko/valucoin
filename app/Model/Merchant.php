<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{
    use SoftDeletes;
    protected $table = "msmerchant";
    protected $primaryKey = "merchant_id";

    public function balance(){
    	return $this->hasMany('App\Model\Balance','merchant_id','merchant_id');
    }
}