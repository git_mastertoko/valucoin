<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    
    protected $table = "msuser";
    protected $primaryKey = "user_id";

    public function balance(){
    	return $this->hasMany('App\Model\Balance','user_id','user_id');
    }
    public function buy(){
    	return $this->hasMany('App\Model\BuyPoint','user_id','user_id');
    }
    public function sell(){
    	return $this->hasMany('App\Model\SellPoint','user_id','user_id');
    }
    public function buypair(){
        return $this->hasMany('App\Model\BuyValucoinPair','user_id','user_id');
    }
    public function referral(){
        return $this->hasMany('App\Model\Referral','user_id','user_id');
    }
}