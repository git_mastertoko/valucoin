<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForgotPassword extends Model
{
    use SoftDeletes;
    protected $table = "forgot_password";
    protected $primaryKey = "forgot_password_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }
}