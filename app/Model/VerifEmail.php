<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VerifEmail extends Model
{
    use SoftDeletes;
    protected $table = "verif_email";
    protected $primaryKey = "verif_email_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }
}