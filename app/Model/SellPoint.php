<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SellPoint extends Model
{
    use SoftDeletes;
    protected $table = "trs_sell_point";
    protected $primaryKey = "sell_point_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }

    public function merchant(){
    	return $this->belongsTo('App\Model\Merchant','merchant_id','merchant_id');
    }
}