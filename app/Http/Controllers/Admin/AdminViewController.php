<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BuyValucoinPair;
use App\Model\BuyPoint;
use App\Model\Airdrop;
use App\Model\User;
use Crypt;
use Auth;

class AdminViewController extends Controller
{   
    public function __construct(){
        $this->middleware('adminview');
    }
    public function index(){
        if(Auth::user()->user_id == 1){
            $airdrop=Airdrop::where('deleted_at',NULL)->get();
            $detail="";
            $listaccount="";
            foreach ($airdrop as $airdrop) {
                $crypt = Crypt::encrypt($airdrop->airdrop_id);
                $detail = "<a href='".url('detail/'.$crypt)."'><button class='btn btn-info edit_btn' id=".$airdrop->airdrop_id.">Detail</button></a>";
                $listaccount = $listaccount."<tr>
                <td></td>
                <td>".$airdrop->email."</td>
                <td>".$detail."</td>
                </tr>";
            }

            $content = "
            <div class='boxs'>
                <div class='box-header with-border'>

                </div>
                <div class='box-body'>
                    <div class='col-sm-12 wrapper2'>
                        <table class='table table-bordered table-striped dataTable table-responsive fixed'>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                ".$listaccount."
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class='modal' id='confirmDelete'>
                <div class='modal-dialog' tabindex='-1'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button class='close' aria-label='Close' data-dismiss='modal' type='button'>
                                <span aria-hidden='true'>×</span>
                            </button>
                            <h4 class='modal-title'>Hapus Bahan</h4>
                        </div>
                        <div class='modal-body'>
                            <div class='form-group'>
                                <div><span class='fontWeightNormal'>Apakah Anda yakin ingin menghapus Bahan ini?</span></div>
                                <div><i class='warnaRed'>*Harga yang termasuk dalam Bahan dan Product ini juga akan terhapus.</i>
                                </div>
                            </div>
                        </div>
                        <div class='modal-footer'>
                            <input type='hidden' id='id' name='id'>
                            <button class='btn btn-default' data-dismiss='modal' type='button'>Tidak</button>
                            <a class='link'><button class='btn btn-danger submit' type='submit' id='btnsend' value=''><span class='glyphicon glyphicon-trash marginRight2' aria-hidden='true'></span>Hapus Bahan</button></a>
                        </div>
                        </form>
                    </div>
                </div>
            </div>";
            $user=User::where('deleted_at',NULL)->count();
            $trx=BuyPoint::where('deleted_at',NULL)->count();
            return view('Admin.index',compact('content','user','trx'));
        }
    	else{
            return redirect('airdrop');
        }
    }

    public function detailview($id){


        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $airdrop = Airdrop::where('airdrop_id',$decrypt)->first();
            $user=User::where('user_id',$airdrop->user_id)->first();

            return view('Admin.detail',compact('airdrop','user'));
        }
        else{
            return redirect('airdrop');
        }
    }


    public function packageview(){
        if(Auth::user()->user_id == 1){
            $package=BuyValucoinPair::where('status',0)->get();
            $sent=""; 
            $listaccount="";
            foreach ($package as $package) {
                $crypt = Crypt::encrypt($package->buy_valucoin_id);
                $sent = "<a href='".url('packagese/'.$crypt)."'><button class='btn btn-info edit_btn' id=".$package->buy_valucoin_id.">SENT</button></a>";
                
                $listaccount = $listaccount."<tr>
                <td></td>
                <td>".$package->tx_code."</td>
                <td>".$package->txid."</td>
                <td>".$package->valu_coin."</td>
                <td>".$package->pair_coin.$package->pair_coin_name."</td>
                <td>".$sent."</td>
                </tr>";
            }

            $content = "
            <div class='boxs'>
                <div class='box-header with-border'>

                </div>
                <div class='box-body'>
                    <div class='col-sm-12 wrapper2'>
                        <table class='table table-bordered table-striped dataTable table-responsive fixed'>
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Transaction Code</th>
                                    <th>TX ID</th>
                                    <th>Total Coin</th>
                                    <th>Pair</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                ".$listaccount."
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class='modal' id='confirmDelete'>
                <div class='modal-dialog' tabindex='-1'>
                    <div class='modal-content'>
                        <div class='modal-header'>
                            <button class='close' aria-label='Close' data-dismiss='modal' type='button'>
                                <span aria-hidden='true'>×</span>
                            </button>
                            <h4 class='modal-title'>Hapus Bahan</h4>
                        </div>
                        <div class='modal-body'>
                            <div class='form-group'>
                                <div><span class='fontWeightNormal'>Apakah Anda yakin ingin menghapus Bahan ini?</span></div>
                                <div><i class='warnaRed'>*Harga yang termasuk dalam Bahan dan Product ini juga akan terhapus.</i>
                                </div>
                            </div>
                        </div>
                        <div class='modal-footer'>
                            <input type='hidden' id='id' name='id'>
                            <button class='btn btn-default' data-dismiss='modal' type='button'>Tidak</button>
                            <a class='link'><button class='btn btn-danger submit' type='submit' id='btnsend' value=''><span class='glyphicon glyphicon-trash marginRight2' aria-hidden='true'></span>Hapus Bahan</button></a>
                        </div>
                        </form>
                    </div>
                </div>
            </div>";
            $user=User::where('deleted_at',NULL)->count();
            $trx=BuyPoint::where('deleted_at',NULL)->count();
            return view('Admin.package',compact('content','user','trx'));
        }
        else{
            return redirect('airdrop');
        }
    }
}
