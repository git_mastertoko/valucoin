<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use App\Model\Airdrop;
use App\Model\BuyValucoinPair;
use App\Model\User;
use Auth;
use Crypt;

class AdminSendValuController extends Controller
{
	public function SendAirdrop($airdropid){
       $code  = 'AS';
        $year  = date('y');
        $month = date('m');
        $day   = date('d');
        $id    = '';
        $error = false;

        $decrypt = Crypt::decrypt($airdropid);
        $airdrop = Airdrop::where('airdrop_id',$decrypt)->first();
        $user = User::where('user_id',$airdrop->user_id)->first();
        $data = Airdrop::where('user_id',$user->user_id)->first();

        if($data == null){
            $id = 1;            
        }
        else{
            $last = substr($data->tx_code_airdrop,-4);
            $tanggal = substr($data->tx_code_airdrop, 3 ,6);
            if($tanggal == $year.$month.$day){
                $id = (int)$last +1;
            }
            else{
                $id = 1;
            }
        }
        $newid = $code.$year.$month.$day.sprintf("%04d", $id);

      
            $url ='https://beta.wavesplatform.com/#send/DZ7GuCkCZ37NDMJUKCEQvL3YH279T4LjqJj2KbTw15xT?recipient='.$data->wave_address.'&amount='.$user->total_deposit_valu.'&referrer=https://token.valucoin.co/valu-airdrop&strict';

        return Redirect::away($url);
		}


	public function txairdrop(){
    	if($_GET['txId'] == true){
    		$url = 'https://nodes.wavesplatform.com/transactions/info/'.$_GET['txId'];

			$options = array(
			    'http' => array(
			        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			        'method'  => 'GET',
			    )
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			if ($result === FALSE) { /* Handle error */ }

	        $json = json_decode($result);
	        $sender    = $json->sender;
	        $recipient = $json->recipient;
	        $fee 	   = $json->fee;
	        $id 	   = $json->id;

	        $user = User::where('user_waves_address',$sender)->first();
	        $airdrop = Airdrop::where('user_id',$user->user_id)->where('tx_id_airdrop','')->first();
	        
	        $airdrop->tx_id_airdrop = $id;
	        $airdrop->save();

	        return redirect('/adminview');
    	}
    	else{
    		return redirect('/adminview');
    	}
    }

    public function SendPackageValu($packagevalu){
        //VBT(Valu Buy Transaction)
        $code  = 'VBT';
        $year  = date('y');
        $month = date('m');
        $day   = date('d');
        $id    = '';
        $error = false;

        $decrypt=Crypt::decrypt($packagevalu);
        $valu= BuyValucoinPair::where('buy_valucoin_id',$decrypt)->first();
        $user = User::where('user_id',$valu->user_id)->first();
         if($valu == null){
            $id = 1;            
        }
        else{
            $last = substr($valu->tx_code_send,-4);
            $tanggal = substr($valu->tx_code_send, 3 ,6);
            if($tanggal == $year.$month.$day){
                $id = (int)$last +1;
            }
            else{
                $id = 1;
            }
        }
        $newid = $code.$year.$month.$day.sprintf("%04d", $id);

      
            $url ='https://beta.wavesplatform.com/#send/DZ7GuCkCZ37NDMJUKCEQvL3YH279T4LjqJj2KbTw15xT?recipient='.$user->user_waves_address.'&amount='.$valu->valu_coin.'&referrer=https://token.valucoin.co/valu-package&strict';

        return Redirect::away($url);
    }

    public function txpackage(){
        if($_GET['txId'] == true){
            $url = 'https://nodes.wavesplatform.com/transactions/info/'.$_GET['txId'];

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'GET',
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) { /* Handle error */ }

            $json = json_decode($result);
            $sender    = $json->sender;
            $recipient = $json->recipient;
            $fee       = $json->fee;
            $id        = $json->id;

            $user = User::where('user_waves_address',$sender)->first();
            $packagevalu = BuyValucoinPair::where('user_id',$user->user_id)->where('txid_send','')->first();
            
            $packagevalu->txid_send = $id;
            $packagevalu->save();

            return redirect('/adminpackage');
        }
        else{
            return redirect('/adminpackage');
        }
    }


}
