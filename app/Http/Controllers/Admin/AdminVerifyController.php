<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Airdrop;
use App\Model\User;
use App\Model\Balance;
use App\Model\VerifEmail;
use Auth;
use Crypt;

class AdminVerifyController extends Controller
{
    public function email($id){
        if(Auth::user()->user_id == 1){
        	$decrypt = Crypt::decrypt($id);
        	$email=Airdrop::where('airdrop_id',$decrypt)->first();
        	$stats=Airdrop::where('airdrop_id',$decrypt)->update([
        		'email_verify'	=> 2
        	]);
            
            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu +10
            ]);

        	return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function address($id){
        if(Auth::user()->user_id == 1){
        	$decrypt = Crypt::decrypt($id);
        	$address=Airdrop::where('airdrop_id',$decrypt)->first();
        	$stats= Airdrop::where('airdrop_id',$decrypt)->update([
        		'wave_address_verify'	=> 2
        	]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu +10
            ]);

        	return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function linkfb($id){
        if(Auth::user()->user_id == 1){
        	$decrypt = Crypt::decrypt($id);
        	$address=Airdrop::where('airdrop_id',$decrypt)->first();
        	$stats= Airdrop::where('airdrop_id',$decrypt)->update([
        		'facebook_profile_link_verify'	=> 2
        	]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu +10
            ]);
        	return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function postfb($id){
        if(Auth::user()->user_id == 1){
        	$decrypt = Crypt::decrypt($id);
        	$address=Airdrop::where('airdrop_id',$decrypt)->first();
        	$stats= Airdrop::where('airdrop_id',$decrypt)->update([
        		'facebook_post_link_verify'	=> 2
        	]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu +20
            ]);
        	return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function linktweet($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'twitter_profile_link_verify' => 2
            ]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu +10
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function posttweet($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'twitter_post_link_verify' => 2
            ]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu +20
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function telegram($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'telegram_account_verify' => 2
            ]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu +10
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function youtube($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'youtube_account_verify' => 2
            ]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu +10
            ]);

            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function remail($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $email=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats=Airdrop::where('airdrop_id',$decrypt)->update([
                'email_verify'  => 3
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function raddress($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'wave_address_verify'   => 3
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function rlinkfb($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'facebook_profile_link_verify'  => 3
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function rpostfb($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'facebook_post_link_verify' => 3
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function rlinktweet($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'twitter_profile_link_verify' => 3
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function rposttweet($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'twitter_post_link_verify' => 3
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function rtelegram($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'telegram_account_verify' => 3
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function ryoutube($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'youtube_account_verify' => 3
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function uemail($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $email=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats=Airdrop::where('airdrop_id',$decrypt)->update([
                'email_verify'  => 1
            ]);
            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu -10
            ]);

            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function uaddress($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'wave_address_verify'   => 1
            ]);
            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu -10
            ]);
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function ulinkfb($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'facebook_profile_link_verify'  => 1
            ]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu -10
            ]);

            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function upostfb($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'facebook_post_link_verify' => 1
            ]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu -20
            ]);

            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function ulinktweet($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'twitter_profile_link_verify' => 1
            ]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu -10
            ]);

            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function uposttweet($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'twitter_post_link_verify' => 1
            ]);

            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu -20
            ]);

            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function utelegram($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'telegram_account_verify' => 1
            ]);
            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu -10
            ]);

            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function uyoutube($id){
        if(Auth::user()->user_id == 1){
            $decrypt = Crypt::decrypt($id);
            $address=Airdrop::where('airdrop_id',$decrypt)->first();
            $stats= Airdrop::where('airdrop_id',$decrypt)->update([
                'youtube_account_verify' => 1
            ]);
            $lasttotal=User::where('user_id',$address->user_id)->first();
            $plus=User::where('user_id',$address->user_id)->update([
                'total_deposit_valu'    =>$lasttotal->total_deposit_valu -10
            ]);
            
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }

    public function delete($id){
        if(Auth::user()->user_id == 1){
            $airdrop = Airdrop::where('user_id',$id)->forcedelete();
            $balance = Balance::where('user_id',$id)->forcedelete();
            $verifemail = VerifEmail::where('user_id',$id)->forcedelete();
            $user = User::where('user_id',$id)->forcedelete();
            return redirect()->back();
        }
        else{
            return redirect('airdrop');
        }
    }
}
