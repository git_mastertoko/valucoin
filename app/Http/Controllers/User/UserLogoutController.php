<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use Auth;

class UserLogoutController extends Controller
{
    public function UserLogout(){
        Auth::logout();
    	return redirect('/');
    }
}