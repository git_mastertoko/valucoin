<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ForgotPassword;
use App\Model\User;
use Mail;
use Hash;
use Crypt;
use Session;

class UserNewPassword extends Controller
{
    public function get($email, $token){
    	$email = Crypt::decrypt($email);
        $dataUser = User::where('user_email',$email)->first();
        if($dataUser == false){
            $error   = true;
            return view('User/userNewPassword',compact('error','email'));
        }
        else{
            $dataForgot = ForgotPassword::where('email',$email)->where('token',$token)->first();
            if($dataForgot == false){
                $error   = true;
                return view('User/userNewPassword',compact('error','email'));
            }
            else{
                $error   = false;
                return view('User/userNewPassword',compact('error','email'));
            }
        }
    }

    public function post(Request $request){
        $email       = $request->email;
        $newPassword = $request->password;

        $dataUser = User::where('user_email',$email)->first();
        if($dataUser == false){
            $error   = false;
            return view('User/userNewPassword',compact('error','email'));
        }
        else{
            $dataUser->user_password = Hash::make($request->password);
            $dataUser->save();

            $forgot = ForgotPassword::where('email',$request->email)->first();
            $forgot->forcedelete();

            $message = "New Password Success";
            return view('User/userRegister',compact('message'));
        }
    }
}