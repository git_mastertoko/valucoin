<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Merchant;
use App\Model\Balance;
use App\Model\CoinTrade;
use App\Model\BuyPoint;
use Redirect;
use Auth;

class UserTradeController extends Controller
{
    public function tradeCoinToPoint(Request $request){
        $code  = 'BPO';
        $year  = date('y');
        $month = date('m');
        $day   = date('d');
        $id    = '';
        $error = false;

        $data = BuyPoint::where('user_id',Auth::user()->user_id)->orderBy('buy_point_id','desc')->first();

        if($data == null){
            $id = 1;            
        }
        else{
            $last = substr($data->buy_point_code,-4);
            $tanggal = substr($data->buy_point_code, 3 ,6);
            if($tanggal == $year.$month.$day){
                $id = (int)$last +1;
            }
            else{
                $id = 1;
            }
        }
        $newid = $code.$year.$month.$day.sprintf("%04d", $id);

        $trade_id    = $request->trade_id;
        $merchant_id = $request->merchant_id;

        $trade = CoinTrade::where('trade_id',$trade_id)->first();

        $buyPoint = new BuyPoint;
        $buyPoint->user_id          = Auth::user()->user_id;
        $buyPoint->merchant_id      = $merchant_id;
        $buyPoint->sell_valu_total  = $trade->valu_coin;
        $buyPoint->buy_point_total  = $trade->valu_point;
        $buyPoint->buy_point_code   = $newid;
        $buyPoint->txid             = "";
        $buyPoint->save();

    	$url = 'https://beta.wavesplatform.com/#send/DZ7GuCkCZ37NDMJUKCEQvL3YH279T4LjqJj2KbTw15xT?recipient=3P5Yt3aYH8bbCS1Y8YQoa9kn2iaxqiKMxoD&amount='.$trade->valu_coin.'&referrer=https://token.valucoin.co/valu-payment&strict';
        return Redirect::away($url);
    }
}