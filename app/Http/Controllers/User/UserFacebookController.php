<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Merchant;
use App\Model\Balance;
use App\Model\Airdrop;
use Session;
use Validator;
use Hash;
use Auth;

class UserFacebookController extends Controller
{
    public function facebook($email, $fullname, $id){
        if($id == "undefined"){
            return redirect('/');
        }
        else{
            if($email == "undefined"){
                $message = "Email undefined.";
                return view('User/userRegister',compact('message'));
            }
            else{
                $password = mt_rand(1000000,99999999);
                $dataUser = User::where('user_email',$email)->first();
                if($dataUser == false){
                    $register = new User;
                    $register->user_account_name             = $fullname;
                    $register->user_password                 = Hash::make($password);
                    $register->user_email                    = $email;
                    $register->user_verif                    = 1;
                    $register->user_total_balance_merchant   = 0;
                    $register->user_total_balance_customer   = 0;
                    $register->user_waves_address            = '';
                    $register->referral_link_generate        = '';
                    $register->referral_link_receive         = '';
                    $register->created_by                    = $fullname;
                    $register->save();

                    $merchant = Merchant::where('deleted_at',NULL)->get();
                    foreach ($merchant as $merchant) {
                        $addBalance = new Balance;
                        $addBalance->user_id                   = $register->user_id;
                        $addBalance->merchant_id               = $merchant->merchant_id;
                        $addBalance->merchant_link_unique_name = '';
                        $addBalance->balance_merchant          = 0;
                        $addBalance->balance_customer          = 0;
                        $addBalance->merchant_verify           = 0;
                        $addBalance->save();
                    }
                    
                    $airdrop = new Airdrop;
                    $airdrop->user_id                          = $register->user_id;
                    $airdrop->email                            = $email;
                    $airdrop->wave_address                     = "";
                    $airdrop->facebook_profile_link            = "";
                    $airdrop->facebook_post_link               = "";
                    $airdrop->date                             = date("Y-m-d");
                    $airdrop->twitter_profile_link             = "";
                    $airdrop->twitter_post_link                = "";
                    $airdrop->telegram_account                 = "";
                    $airdrop->youtube_account                  = "";
                    $airdrop->email_verify                     = 0;
                    $airdrop->wave_address_verify              = 0;
                    $airdrop->facebook_profile_link_verify     = 0;
                    $airdrop->facebook_post_link_verify        = 0;
                    $airdrop->twitter_profile_link_verify      = 0;
                    $airdrop->twitter_post_link_verify         = 0;
                    $airdrop->telegram_account_verify          = 0;
                    $airdrop->youtube_account_verify           = 0;
                    $airdrop->save();

                    Auth::login($register);
                    return redirect('/airdrop');
                }
                else{
                    Auth::login($dataUser);
                    return redirect('/airdrop');
                }
            }
        }
    }
}