<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use Validator;
use Auth;
use Hash;
use Session;

class UserLoginController extends Controller
{
    public function UserLoginView(){
    	return view('auth.login');
    }

    public function UserLogin(Request $request){
        $message = [
            'login_password.required'     => 'Password can not be empty',
            'login_password.min'          => 'Minimum length of password is 8',
            'login_email.required'        => 'Email can not be empty',
            'login_email.email'           => 'Wrong email format',
        ];
        $Validator = Validator::make($request->all(),[
            'login_email'        => 'required|email',
            'login_password'     => 'required|min:8',
        ],$message);

        if($Validator->passes()){
        	$email 		= $request->login_email;
            $password 	= $request->login_password;
            $hasPass 	= Hash::make($password);
            $users = User::where('user_email',$email)->first();

            if($users == false){
                $message = "Email or Password wrong.";
                return view('User/userRegister',compact('message'));
            }
            else if($users->user_verif == 0){
                $message = "Your account is not yet verified. Check your email.";
                return view('User/userRegister',compact('message'));
            }
            else{
            	if(Hash::check($password,$users->user_password)){
                    Auth::login($users);
                    if($users->user_id == 1){
                        return redirect('/adminview');
                    }
                    else{
                        return redirect('/airdrop');
                    }
                }
                else{
                    $message = "Email or Password wrong.";
                    return view('User/userRegister',compact('message'));
                }
            }
        }
        else{
            return redirect()->back()->withErrors($Validator)->withInput();
        }
    }
}