<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CoinPair;
use App\Model\BuyValucoinPair;
use App\Model\User;
use Crypt;
use Auth;
use Redirect;

class UserBuyValuController extends Controller
{
    public function index(){
    	$pair  = CoinPair::where('deleted_at',NULL)->get();
        $pair1 = CoinPair::where('deleted_at',NULL)->get();
        $pair2 = CoinPair::where('deleted_at',NULL)->get();
    	return view('User.userBuyValuIndex',compact('pair','pair1','pair2'));
    }

    public function pair(Request $request){
        $code  = 'BPO';
        $year  = date('y');
        $month = date('m');
        $day   = date('d');
        $id    = '';
        $error = false;

        $data = BuyValucoinPair::where('user_id',Auth::user()->user_id)->orderBy('buy_valucoin_id','desc')->first();

        if($data == null){
            $id = 1;            
        }
        else{
            $last = substr($data->buy_point_code,-4);
            $tanggal = substr($data->buy_point_code, 3 ,6);
            if($tanggal == $year.$month.$day){
                $id = (int)$last +1;
            }
            else{
                $id = 1;
            }
        }
        $newid = $code.$year.$month.$day.sprintf("%04d", $id);

        $pair_id        = $request->pair_id;
        $pair_coin_name = $request->pair_coin_name;
        $pair = CoinPair::where('pair_id',$pair_id)->first();
        $pair_coin = CoinPair::where('pair_id',$pair_id)->value($pair_coin_name);

        $buyValu = new BuyValucoinPair;
        $buyValu->user_id           = Auth::user()->user_id;
        $buyValu->valu_coin         = $pair->valucoin;
        $buyValu->valu_bonus        = $pair->valucoin_bonus;
        $buyValu->pair_coin_name    = $pair_coin_name;
        $buyValu->pair_coin         = $pair_coin;
        $buyValu->tx_code           = $newid;
        $buyValu->txid              = "";
        $buyValu->save();

        if($pair_coin_name == "Waves"){
            $url = 'https://beta.wavesplatform.com/#send/WAVES?recipient=3P5Yt3aYH8bbCS1Y8YQoa9kn2iaxqiKMxoD&amount='.$pair_coin.'&referrer=https://token.valucoin.co/valu-pair&strict';
        }
        else if($pair_coin_name == "btc"){
            $url = 'https://beta.wavesplatform.com/#send/8LQW8f7P5d5PZM7GtZEBgaqRPGSzS3DfPuiXrURJ4AJS?recipient=3P5Yt3aYH8bbCS1Y8YQoa9kn2iaxqiKMxoD&amount='.$pair_coin.'&referrer=https://token.valucoin.co/valu-pair&strict';
        }
        else{
            $url = 'https://beta.wavesplatform.com/#send/474jTeYx2r2Va35794tCScAXWJG9hU2HcgxzMowaZUnu?recipient=3P5Yt3aYH8bbCS1Y8YQoa9kn2iaxqiKMxoD&amount='.$pair_coin.'&referrer=https://token.valucoin.co/valu-pair&strict';
        }

        return Redirect::away($url);
    }


     public function sendcoin(Request $request,$id){
        $code  = 'SPO';
        $year  = date('y');
        $month = date('m');
        $day   = date('d');
        $id    = '';
        $error = false;
        $decrypt= Crypt::decrypt($id);
        $data = BuyValucoinPair::where('buy_valucoin_id',$decrypt)->first();
        $user= User::where('user_id',$data->user_id)->first();
        if($data == null){
            $id = 1;            
        }
        else{
            $last = substr($data->tx_code_send,-4);
            $tanggal = substr($data->tx_code_send, 3 ,6);
            if($tanggal == $year.$month.$day){
                $id = (int)$last +1;
            }
            else{
                $id = 1;
            }
        }
        $newid = $code.$year.$month.$day.sprintf("%04d", $id);

        $update= BuyValucoinPair::wherewhere('buy_valucoin_id',$data->buy_valucoin_id)->update([
          'tx_code_send' => $newid
        ]);

        $url = 'https://beta.wavesplatform.com/#send/DZ7GuCkCZ37NDMJUKCEQvL3YH279T4LjqJj2KbTw15xT?recipient='.$user->user_waves_address.'&amount='.$data->valu_coin.'&referrer=https://token.valucoin.co/sent-valu&strict';

        return Redirect::away($url);
    }

}