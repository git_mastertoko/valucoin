<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Crypt;

class UserEmailController extends Controller
{
    public function apiemail(){
        $veriftoken = str_random(50);
        $cryptEmail = Crypt::encrypt('valupoint@valucoin.co');
        $url = "";//port for smtp

        $path = "https://token.valucoin.co/verif"."/".$cryptEmail."_".$veriftoken;

        $data['name']    = 'Valu Coin';
        $data['path']    = $path;
        $data['email']   = 'valupoint@valucoin.co';
        $data['email_sendto'] = 'valupoint@valucoin.co';
        $data['buat_subject'] = "Verify Your ValuCoin Account";

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        // if ($result === FALSE) { /* Handle error */ }

        // Mail::send('email.verif_email', $data, function ($message) use($email_sendto, $buat_subject) {
        //     $message->from('info@valucoin.co')->to($email_sendto)->subject($buat_subject);
        // });
    }

    public function email(){
    	$veriftoken = str_random(50);
    	$cryptEmail = Crypt::encrypt('valupoint@valucoin.co');

    	$path = "https://token.valucoin.co/verif"."/".$cryptEmail."_".$veriftoken;
        $data['name']   = 'Valu Coin';
        $data['path']   = $path;
        $data['email']  = 'valupoint@valucoin.co';

        $email_sendto = 'valupoint@valucoin.co';
        $buat_subject = "Verify Your ValuCoin Account";

        Mail::send('email.verif_email', $data, function ($message) use($email_sendto, $buat_subject) {
            $message->from('info@valucoin.co')->to($email_sendto)->subject($buat_subject);
        });
    }
}