<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Airdrop;
use Session;
use Auth;

class UserFormController extends Controller
{
  public function index(){
    $message = "";
  	return view('User.userRegister',compact('message'));
  }

  public function submit(Request $request){
    // $token = $request->input('g-recaptcha-response');
    // if($token){
      $airdrop1 = Airdrop::where('user_id',Auth::User()->user_id)->first();
    	$airdrop = Airdrop::where('user_id',Auth::User()->user_id)->update([
        'facebook_profile_link' => $request->facebook_profile_link,
        'facebook_post_link'    => $request->facebook_post_link,
        'twitter_profile_link'  => $request->twitter_profile_link,
        'twitter_post_link'     => $request->twitter_post_link,
        'telegram_account'      => $request->telegram_account,
        'youtube_account'       => $request->youtube_account
      ]);
      if($request->facebook_profile_link != "" && $airdrop1->facebook_profile_link_verify != 2){
        $airdrop= Airdrop::where('user_id',Auth::User()->user_id)->update([
          'facebook_profile_link_verify' => 1
        ]);
      }
      if($request->facebook_post_link != "" && $airdrop1->facebook_post_link_verify != 2){
         $airdrop= Airdrop::where('user_id',Auth::User()->user_id)->update([
          'facebook_post_link_verify' => 1
        ]);
      }
      if($request->twitter_profile_link != "" && $airdrop1->twitter_profile_link_verify != 2){
        $airdrop= Airdrop::where('user_id',Auth::User()->user_id)->update([
          'twitter_profile_link_verify' => 1
        ]);
      }
      if($request->twitter_post_link != "" && $airdrop1->twitter_post_link_verify != 2){
        $airdrop= Airdrop::where('user_id',Auth::User()->user_id)->update([
          'twitter_post_link_verify' => 1
        ]);
      }
      if($request->telegram_account != "" && $airdrop1->telegram_account_verify != 2){
        $airdrop= Airdrop::where('user_id',Auth::User()->user_id)->update([
          'telegram_account_verify' => 1
        ]);
      }
      if($request->youtube_account != "" && $airdrop1->youtube_account_verify != 2){
        $airdrop= Airdrop::where('user_id',Auth::User()->user_id)->update([
          'youtube_account_verify' => 1
        ]);
      }

      $message = 'Thank You for submit.';
      $airdrop = Airdrop::where('user_id',Auth::User()->user_id)->first();
      return view('User.userAirdropViewInside',compact('message','airdrop'));
    // }
    // else{
    //   Session::flash('flash_message_error','Wrong Captcha');
    //   return redirect('airdrop'); 
    // }
  }
}