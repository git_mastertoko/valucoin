<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BuyPoint;
use App\Model\User;
use App\Model\CoinTrade;

class UserPaymentController extends Controller
{
	// For Buy Point
    public function payment(){
    	if($_GET['txId'] == true){
    		$url = 'https://nodes.wavesplatform.com/transactions/info/'.$_GET['txId'];

			$options = array(
			    'http' => array(
			        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			        'method'  => 'GET',
			    )
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			if ($result === FALSE) { /* Handle error */ }

	        $json = json_decode($result);
	        $sender    = $json->sender;
	        $recipient = $json->recipient;
	        $amount    = $json->amount;
	        $id 	   = $json->id;

	        $user = User::where('user_waves_address',$sender)->first();
	        $buyPoint = BuyPoint::where('user_id',$user->user_id)->where('sell_valu_total',$amount)->where('txid','')->orderBy('buy_point_id','desc')->first();
	        $buyPoint->txid = $id;
	        $buyPoint->save();

	        $user->user_total_balance_merchant = $user->user_total_balance_merchant + $amount;
	        $user->save();

	        return redirect('/home');
    	}
    	else{
    		return redirect('/home');
    	}
    }
    // For Buy ValuCoin
    public function pair(){
    	if($_GET['txId'] == true){
    		$url = 'https://nodes.wavesplatform.com/transactions/info/'.$_GET['txId'];

			$options = array(
			    'http' => array(
			        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			        'method'  => 'GET',
			    )
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			if ($result === FALSE) { /* Handle error */ }

	        $json = json_decode($result);
	        $sender    = $json->sender;
	        $recipient = $json->recipient;
	        $fee 	   = $json->fee;
	        $id 	   = $json->id;

	        $user = User::where('user_waves_address',$sender)->first();
	        $buyPair = BuyValucoinPair::where('user_id',$user->user_id)->where('pair_coin',$fee)->where('txid','')->orderBy('buy_valucoin_id','desc')->first();
	        $buyPair->txid = $id;
	        $buyPair->save();

	        return redirect('/home');
    	}
    	else{
    		return redirect('/home');
    	}
    }

    // For Check TxID
    public function checkTX($txid){
		$url = 'https://nodes.wavesplatform.com/transactions/info/'.$txid;

		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'GET',
		    )
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		if ($result === FALSE) { /* Handle error */ }

        $json = json_decode($result);
        $sender    = $json->sender;
        $orderType = $json->orderType;
        $amount    = $json->amount;
        $id 	   = $json->id;
        $valu_code = $json->order1->assetPair->priceAsset;
        $pair_code = $json->order1->assetPair->amountAsset;

        if($orderType == "buy" && $valu_code == "DZ7GuCkCZ37NDMJUKCEQvL3YH279T4LjqJj2KbTw15xT"){
        	if($pair_code == "8LQW8f7P5d5PZM7GtZEBgaqRPGSzS3DfPuiXrURJ4AJS"){
        		$pair_name = "btc";
        	}
        	else if($pair_code == "474jTeYx2r2Va35794tCScAXWJG9hU2HcgxzMowaZUnu"){
        		$pair_name = "eth";
        	}
        	else{
        		$pair_name = "waves";
        	}

        	$user = User::where('user_waves_address',$sender)->first();

        	$code  = 'BPO';
	        $year  = date('y');
	        $month = date('m');
	        $day   = date('d');
	        $id    = '';
	        $error = false;

	        $data = BuyValucoinPair::where('user_id',$user->user_id)->orderBy('buy_valucoin_id','desc')->first();

	        if($data == null){
	            $id = 1;            
	        }
	        else{
	            $last = substr($data->buy_point_code,-4);
	            $tanggal = substr($data->buy_point_code, 3 ,6);
	            if($tanggal == $year.$month.$day){
	                $id = (int)$last +1;
	            }
	            else{
	                $id = 1;
	            }
	        }
	        $newid = $code.$year.$month.$day.sprintf("%04d", $id);

	        $buyValu = new BuyValucoinPair;
	        $buyValu->user_id           = $user->user_id;
	        $buyValu->valu_coin         = $amount;
	        $buyValu->valu_bonus        = $amount;
	        $buyValu->pair_coin_name    = $pair_name;
	        $buyValu->pair_coin         = $price;
	        $buyValu->tx_code           = $newid;
	        $buyValu->txid              = $id;
	        $buyValu->save();
        }

        return redirect('/home');
    }
}
