<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Airdrop;
use Auth;
use Response;

class UserAirdropViewController extends Controller
{
    public function view(){
      $airdrop = DB::table('msairdrop')->get();
    	return view('User.userAirdropView',compact('airdrop'));
    }

    public function index(){
    	$airdrop = Airdrop::where('user_id',Auth::User()->user_id)->first();
      $message = "";
    	return view('User.userAirdropViewInside',compact('airdrop','message'));
   	}

   	public function terms_conditions(){
   		$file = "./File/terms_and_conditions_ValuCoin.pdf";
        return Response::download($file);
   	}

    public function privacy_policy(){
      $file = "./File/privacy_policy_ValuCoin.pdf";
        return Response::download($file);
    }
}