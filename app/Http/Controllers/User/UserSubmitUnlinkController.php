<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Balance;
use Crypt;
use Auth;

class UserSubmitUnlinkController extends Controller
{
    public function submit(Request $request){
    	$merchant = Balance::where('user_id',Auth::user()->user_id)->where('merchant_id',Crypt::decrypt($request->merchant_id))->first();
    	$merchant->merchant_link_unique_name = "";
    	$merchant->merchant_verify 			 = 0;
    	$merchant->save();

    	return redirect('/home');
    }
}
