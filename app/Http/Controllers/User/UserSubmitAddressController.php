<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use Auth;

class UserSubmitAddressController extends Controller
{
    public function addresssubmit(Request $request){
    	$address = User::Where('user_id',Auth::User()->user_id)->update([
    		'user_waves_address'	=>$request->address
    	]);
    	return redirect('/home');
    }
}
