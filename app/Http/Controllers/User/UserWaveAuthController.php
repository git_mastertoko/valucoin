<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BuyPoint;
use App\Model\User;
use App\Model\CoinTrade;
use App\Model\Airdrop;
use Redirect;
use Session;
use Crypt;
use Auth;

class UserWaveAuthController extends Controller
{
    public function index(){
    	$data 		= $_GET['d'];
    	$signature  = $_GET['s'];
    	$public_key = $_GET['p'];
    	$address    = $_GET['a'];
    	$d = Crypt::decrypt($data);
		$user = User::where('user_id',$d)->first();
        if($user == false){
            $message = 'The link you have entered is incorrect.';
            $airdrop = Airdrop::where('user_id',Auth::User()->user_id)->first();
            return view('User.userAirdropViewInside',compact('message','airdrop'));
        }
        else{
            $dataAddress = Airdrop::where('wave_address',$address)->first();
            if($dataAddress == false){
                $airdrop = Airdrop::where('user_id',$user->user_id)->first();
                $user->user_waves_address = $address;
                $user->save();
                $airdrop->wave_address        = $address;
                $airdrop->wave_address_verify = 2;
                $airdrop->save();

                $lasttotal=User::where('user_id',$airdrop->user_id)->first();
                $plus=User::where('user_id',$airdrop->user_id)->update([
                    'total_deposit_valu'    =>$lasttotal->total_deposit_valu +10
                ]);
                
                $message = 'Register Waves Wallet Success.';
                $airdrop = Airdrop::where('user_id',Auth::User()->user_id)->first();
                return view('User.userAirdropViewInside',compact('message','airdrop'));
            }
            else{
                $message = 'Waves Wallet already used.';
                $airdrop = Airdrop::where('user_id',Auth::User()->user_id)->first();
                return view('User.userAirdropViewInside',compact('message','airdrop'));
            }
        }
    }

    public function login(){
    	$d = Crypt::encrypt(Auth::user()->user_id);
    	$url = 'https://beta.wavesplatform.com#gateway/auth?r=https://token.valucoin.co&n=Valupoint&d='.$d.'&s=/wavesAuth&debug=true';
        return Redirect::away($url);
    }
}
