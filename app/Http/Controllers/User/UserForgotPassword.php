<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ForgotPassword;
use App\Model\User;
use Mail;
use Crypt;
use Session;

class UserForgotPassword extends Controller
{
    public function get(){
        return view('User.userForgotPassword');
    }

    public function post(Request $request){
    	$email = $request->email;
        $dataUser = User::where('user_email',$email)->first();
        if($dataUser == false){
            Session::flash('flash_message','Email does not exists');
            return redirect()->back();
        }
        else{
            $dataForgot = ForgotPassword::where('email',$email)->first();
            $token = str_random(50);
            if($dataForgot == false){
                $forgot_password = new ForgotPassword;
                $forgot_password->user_id = $dataUser->user_id;
                $forgot_password->email   = $email;
                $forgot_password->token   = $token;
                $forgot_password->save();
            }
            else{
                $dataForgot->token = $token;
                $dataForgot->save();
            }

            $cryptEmail = Crypt::encrypt($email);
            $path = "https://token.valucoin.co/new_password/".$cryptEmail."_".$token;

            $data['name']     = $dataUser->user_name;
            $data['path']     = $path;
            $data['email']    = $email;
            $data['email_sendto'] = $email;
            $data['buat_subject'] = "Reset Password";

            $url = "";//port for smtp

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            Session::flash('flash_message_success','Forgot Password Success. Check your email to set new password.');
            return redirect()->back();
        }
    }
}