<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\VerifEmail;
use Crypt;

class UserVerifEmailController extends Controller
{
    public function verif($email, $token){
        $email = Crypt::decrypt($email);
        $dataUser = User::where('user_email',$email)->first();
        if($dataUser == false){
            $error   = true;
            $message = "Your email is not found";
            return view('User/userVerif',compact('message'));
        }
        else{
            if($dataUser->user_verif == 1){
                $error   = true;
                $message = "Your Account already verified";
                return view('User/userVerif',compact('message'));
            }
            else{
                $dataVerif = VerifEmail::where('email',$email)->where('token',$token)->first();
                if($dataVerif == false){
                    $error   = true;
                    $message = "Your link is not found";
                    return view('User/userVerif',compact('message'));
                }
                else{
                    $dataUser->user_verif = 1;
                    $dataUser->save();

                    $dataVerif->forcedelete();

                    $error   = false;
                    $message = "Your Account is verified now";
                    return view('User/userVerif',compact('message'));
                }
            }
        }
    }
}