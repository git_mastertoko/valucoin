<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Merchant;
use App\Model\Balance;
use App\Model\Airdrop;
use App\Model\VerifEmail;
use Mail;
use Validator;
use Session;
use Hash;
use Crypt;
use Auth;

class UserRegisterController extends Controller
{
    public function UserRegisterView(){
    	return view('auth.register');
    }
    public function UserMobileRegisterView(){
        return view('User.userMobileRegisterView');
    }
    public function UserRegister(Request $request){
    	$message = [
    		'name.required'			            => 'User name can not be empty',
    		'password.required'				    => 'Password can not be empty',
    		'password.min'						=> 'Minimum length of password is 8',
    		'password.confirmed'				=> 'Password must be confirmed',
            'password_confirmation.required'	=> 'Password confirmation can not be empty',
            'password_confirmation.min'		    => 'Minimum length of password confirmation is 8',
            'email.required'					=> 'Email can not be empty',
           	'email.email'						=> 'Wrong email format',
           	'email.unique'						=> 'Email is already registered'
    	];
    	$Validator = Validator::make($request->all(),[
    		'name'				    => 'required',
    		'email'					=> 'required|unique:msuser,user_email|email',
    		'password'				=> 'required|min:8|confirmed',
    		'password_confirmation'	=> 'required|min:8'
    	],$message);

    	if($Validator->passes()){
    		$register = new User;
            $register->user_account_name             = $request->name;
            $register->user_password                 = Hash::make($request->password);
            $register->user_email                    = $request->email;
            $register->user_verif                    = 0;
            $register->total_deposit_valu            = 10;
            $register->user_total_balance_merchant   = 0;
            $register->user_total_balance_customer   = 0;
            $register->user_waves_address            = '';
            $register->referral_link_generate        = '';
            $register->referral_link_receive         = '';
            $register->created_by                    = $request->name;
    		$register->save();

            $merchant = Merchant::where('deleted_at',NULL)->get();
            foreach ($merchant as $merchant) {
                $addBalance = new Balance;
                $addBalance->user_id                   = $register->user_id;
                $addBalance->merchant_id               = $merchant->merchant_id;
                $addBalance->merchant_link_unique_name = "";
                $addBalance->balance_merchant          = 0;
                $addBalance->balance_customer          = 0;
                $addBalance->merchant_verify           = 0;
                $addBalance->save();
            }
            
            $airdrop = new Airdrop;
            $airdrop->user_id                          = $register->user_id;
            $airdrop->email                            = $request->email;
            $airdrop->wave_address                     = "";
            $airdrop->facebook_profile_link            = "";
            $airdrop->facebook_post_link               = "";
            $airdrop->date                             = date("Y-m-d");
            $airdrop->twitter_profile_link             = "";
            $airdrop->twitter_post_link                = "";
            $airdrop->telegram_account                 = "";
            $airdrop->youtube_account                  = "";
            $airdrop->email_verify                     = 0;
            $airdrop->wave_address_verify              = 0;
            $airdrop->facebook_profile_link_verify     = 0;
            $airdrop->facebook_post_link_verify        = 0;
            $airdrop->twitter_profile_link_verify      = 0;
            $airdrop->twitter_post_link_verify         = 0;
            $airdrop->telegram_account_verify          = 0;
            $airdrop->youtube_account_verify           = 0;
            $airdrop->save();

            $link = User::where('user_id',$register->user_id)->first();
            $link->referral_link_generate = 'https://token.valucoin.co/ref/'.base64_encode($link->user_id);
            $link->save();

            $verif = new VerifEmail;
            $verif->user_id = $register->user_id;
            $verif->email   = $request->email;
            $verif->token   = str_random(50);
            $verif->save();

            $cryptEmail = Crypt::encrypt($verif->email);

            $path = "https://token.valucoin.co/verif/".$cryptEmail."_".$verif->token;
            $data['name']     = $request->name;
            $data['path']     = $path;
            $data['email']    = $request->email;
            $data['email_sendto'] = $request->email;
            $data['buat_subject'] = "Verify Your ValuCoin Account";

            $url = "";//port for smtp

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);

            $message = "Register Success. Check your email to verify your account.";
    		return view('User/userRegister',compact('message'));
    	}
    	else{
    		return redirect()->back()->withErrors($Validator)->withInput();
    	}
    }
}