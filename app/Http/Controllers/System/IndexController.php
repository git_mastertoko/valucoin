<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CoinTrade;
use App\Model\Balance;
use App\Model\Merchant;
use App\Model\BuyPoint;
use Auth;
use Abraham\TwitterOAuth\TwitterOAuth;

class IndexController extends Controller
{
    public function index(){
    	$data1 = CoinTrade::where('trade_status','1')->get();
    	$data2 = CoinTrade::where('trade_status','2')->get();
    	$balanceMerchant = Balance::where('user_id',Auth::user()->user_id)->get();
    	$balanceUser = Balance::where('user_id',Auth::user()->user_id)->get();
        $merchant = Merchant::where('role_name','merchant')->get();
    	return view('System.index',compact('data1','data2','balanceMerchant','balanceUser','merchant'));
    }

    public function cover(){
    	return view('System.cover');
    }

    public function getwaves(Request $request){
    	$trade = new BuyPoint;
        $trade->user_id         = Auth::User()->user_id;
        $trade->merchant_id     = $request->merchant_id;
        $trade->sell_valu_total = $request->valu_coin;
        $trade->buy_point_total = $request->valu_point;
        $trade->buy_point_code  = $request->buy_point_code;
        $trade->save();
        
    }

    public function viewtwitter(){
        $connection = new TwitterOAuth('RUgNRk1Gg9S2tJCq96qCCRvk0', 'neuMhqq2Xk2FBbYaI63rDSx1eBaCjfdHMWAXipAKmMuKtBUh4i');
        $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => 'https://token.valucoin.co/twitter'));
        $_SESSION['oauth_token'] = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
        $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));

        echo "<a href='$url'><img src='twitter-login-blue.png' style='margin-left:4%; margin-top: 4%'></a>";
        //echo $url;
    }

    public function twitter(){
        $connection = new TwitterOAuth('RUgNRk1Gg9S2tJCq96qCCRvk0', 'neuMhqq2Xk2FBbYaI63rDSx1eBaCjfdHMWAXipAKmMuKtBUh4i');
        $access_token = $connection->oauth('oauth/access_token', array('oauth_verifier' => $_GET['oauth_verifier']));
        dd($access_token);
    }
}
