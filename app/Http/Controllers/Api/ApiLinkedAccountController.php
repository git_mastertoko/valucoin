<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Merchant;
use App\Model\Balance;
use Auth;
use Crypt;

class ApiLinkedAccountController extends Controller
{
    public function link($id){
    	// $id = Crypt::decrypt($id);
    	$dataMerchant = Merchant::where('merchant_id',$id)->first();
        $dataBalance = Balance::where('merchant_id',$id)->where('user_id',Auth::user()->user_id)->first();
    	$url = $dataMerchant->merchant_url;

        $merchant_id = $id;
        $user_id     = Auth::user()->user_id;

		$data = array(
			'from_url'	      => 'http://valucoin.co/',
			'account_unique'  => $dataBalance->merchant_link_unique_name,
            'user_id'         => Crypt::encrypt($user_id),
            'merchant_id'     => Crypt::encrypt($merchant_id)
		);

		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data)
		    )
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		if ($result === FALSE) { /* Handle error */ }

        $json = json_decode($result);
        if($json->error == false){
            echo 'success send email';
        }
        else{
            echo 'user not found';
        }
		// return redirect($url);
		// $link = "<script>window.open('$url',width=50,height=50,left=50,top=50)</script>";
		// echo $link;
    }

    public function respon(Request $request){
    	$user_id         = $request['user_id'];
    	$merchant_id  	 = $request['merchant_id'];
    	$verify    		 = $request['verify'];
    	$account_unique  = $request['account_unique'];

        $merchant_id = Crypt::decrypt($merchant_id);
        $user_id     = Crypt::decrypt($user_id);

    	if($verify == 'yes'){
    		$dataBalance = Balance::where('merchant_id',$merchant_id)->where('user_id',$user_id)->first();
    		$dataBalance->merchant_link_unique_name = $account_unique;
    		$dataBalance->merchant_verify 			= 1;
    		$dataBalance->save();

            echo 'verified';
    	}
    	else{
            echo 'not verified';
    	}
    }
}