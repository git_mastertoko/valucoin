<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AdminViewMiddleware
{
    /**
     * Handle an incoming request.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->user_email =="valupoint@valucoin.co"){
        return $next($request);
        }
        else{
            return redirect('/airdrop');
        }
    }
}
