<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>VALUCOIN THE GLOBAL CASHBACK TOKEN</title>
  <link href="imgs/logo.png" rel="shortcut icon" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="google-site-verification" content="jWPAxnvQuYQnpvyPol73km8nuGaoJl5E8wlHDY_bq10" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
  <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/v1526884278587/recaptcha__id.js"></script>
  <script src="https://www.google.com/recaptcha/api.js"></script>
  <meta name="maValidation" content="47604a6984d19e08376e90ac487a8005">
  <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-113709182-2"></script>
</head>
<body>
  <div>
    <table id="customers">
      <tr>
        <th>No</th>
        <th>Email</th>
        <th>Valu Wallet</th>
        <th>Facebook Profile</th>
        <th>Facebook Post Link</th>
        <th>Date</th>
        <th>Verify</th>
      </tr>
      <?php $id = 0; ?>
      @foreach($airdrop as $airdrop)
      <?php $id = $id + 1; ?>
      <tr>
        <td>{{$id}}</td>
        <td>{{$airdrop->email}}</td>
        <td>{{$airdrop->wave_address}}</td>
        <td>{{$airdrop->facebook_profile_link}}</td>
        <td>{{$airdrop->facebook_post_link}}</td>
        <td>{{$airdrop->date}}</td>
        <td>{{$airdrop->verify}}</td>
      </tr>
      @endforeach
    </table>
  </div>
</body>
</html>