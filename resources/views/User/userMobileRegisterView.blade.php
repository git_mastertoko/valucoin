<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>VALUCOIN THE GLOBAL CASHBACK TOKEN</title>
  <link href="imgs/logo.png" rel="shortcut icon" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="google-site-verification" content="jWPAxnvQuYQnpvyPol73km8nuGaoJl5E8wlHDY_bq10" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
  <script js.src= "https://connect.facebook.net/en_US/sdk.js"></script>
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <meta name="google-signin-client_id" content="139050077010-4lq9t4pur2fq7ivelnjqpu2k2kvt828m.apps.googleusercontent.com">
  <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/v1526884278587/recaptcha__id.js"></script>
  <script src="https://www.google.com/recaptcha/api.js"></script>
  <meta name="maValidation" content="47604a6984d19e08376e90ac487a8005">
  <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-113709182-2"></script>
<style media="screen">

.part{
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: -1;
}
}

</style>
</head>

<body style="background-color:#29af4b;">
  <!-- <div id="id03" class="modal-airdrop" style="overflow:hidden;">

    <form class="modal-content-airdrop animate" action="{{url('/airdrop')}}" method="post">
      {{ csrf_field() }}

      <div class="modal-container">
        <fieldset style="padding:10px;">
          <legend>
            Form
            <i class="fa fa-times icon-close-button" onclick="document.getElementById('id03').style.display='none'" style=""></i>
          </legend>

          <div class="form-group">
            <label>Email Address</label>
            <input class="form-control" name="email" type="email" required="" placeholder="Your Email Address">
          </div>
          <div class="form-group">
            <label>Valu Wallet Address</label>
            <input class="form-control" name="wave_address" type="text" required="" placeholder="Your Valu Wallet Address">
          </div>
          <div class="form-group">
            <label>Facebook Profile URL</label>
            <input class="form-control" name="facebook_profile_link" type="text" required="" placeholder="Your Facebook Profile URL">
          </div>
          <div class="form-group">
            <label>Facebook Post Link URL</label>
            <input class="form-control" name="facebook_post_link" type="text" required="" placeholder="Your Facebook Post Link URL">
          </div>
          <div class="form-group">
           <div class="g-recaptcha" data-sitekey="6LevvWAUAAAAALomPcuM89O5b08aNQ2QtJN2r0SV"></div>
         </div>
        </fieldset>

        <div class="hide" id="hide" style="display:none;">
            <span class="glyphicon glyphicon-remove" style="color:red"></span><em>Please Retry the cache</em>
        </div>

        @if(Session::has('flash_message_error'))
        <span class="glyphicon glyphicon-remove" style="color:red"></span><em> {!! session('flash_message_error') !!}</em>
        @endif
        @if(Session::has('flash_message'))
          <span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em>
        @endif
        <button class="btn btn-primary m-btm" type="submit" style="padding:15px;">Submit</button>
      </div>
    </form>
  </div>

  <div> -->


    <!--  -->
<!-- <button id="googleSignIn" class="btn2 btn-Block btn-Google mar_bot" type="button">
  <i class="fa fa-google" aria-hidden="true">
    <span class="btn_tulisan" >Masuk Dengan Google</span>
  </i>
</button>
<div id="my-signin2"></div> -->

    <div id="particles-js"  >
    </div>
    <div class="container" style=" height:99vh;">
@if ($errors->has('email'))
  <div class="alert alert-success"><strong style="color:red">{{ $errors->first('email') }}</strong></div>
@endif
@if(Session::has('flash_message'))
  <div class="alert alert-success"><strong style="color:red">{!! session('flash_message') !!}</strong></div>
@endif
@if(Session::has('flash_message_register'))
  <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message_register') !!}</em></div>
@endif
      <div class="title-content">
        REGISTER
      </div>
      <div class="master">
        <form class="mregister" method="POST" action="{{ url('/register') }}">
       {{ csrf_field() }}

     <center> <label for="name" class="mfont"><b>USERNAME</b></label></center>
      <input type="text" placeholder="Enter Username" name="name" id="name" required>
        <!-- @if ($errors->has('name'))
          <strong>{{ $errors->first('name') }}</strong>
        @endif -->
      <br>

      <center><label for="email" class="mfont"><b>E-MAIL</b></label><br></center>
      <input type="email" placeholder="Enter Email" name="email" id="email" required>
        <!-- @if ($errors->has('email'))
          <strong>{{ $errors->first('email') }}</strong>
        @endif -->
      <br>

      <center><label for="password" class="mfont"><b>PASSWORD</b></label></center>
      <input type="password" placeholder="Enter Password" name="password" id="password" required>
        <!-- @if ($errors->has('password'))
          <strong>{{ $errors->first('password') }}</strong>
        @endif -->
        <br>
        <center> <label for="password_confirmation" class="mfont"><b>PASSWORD CONFIRMATION</b></label></center>
      <input type="password" placeholder="Enter Password Confirmation" name="password_confirmation" id="password_confirmation" required>
        <!-- @if ($errors->has('password_confirmation'))
          <strong>{{ $errors->first('password_confirmation') }}</strong>
        @endif -->
        <br>
      <input type="hidden" id="triggerhiddenregister" value="register"><br>
      <input type="checkbox" name="privacy_policy" id="privacy_policy" value="aggree"> I agree with <a href="/privacy_policy" class="privacy-color">privacy policy</a> and <a href="/terms_conditions" class="privacy-color">terms conditions</a><br><br>
      <button type="submit" id="mregister_button">REGISTER</button>
  </form>
        
  </div>
<script type="text/javascript" src="{{asset('js/particles.js')}}"></script>
<script type="text/javascript" src="{{asset('js/js/app.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(function(){
      $('form').submit(function(event) {
        var privacy_policy = document.getElementById("privacy_policy");
        console.log(privacy_policy.checked);
        var password = document.getElementById("password");
        var password_confirmation = document.getElementById("password_confirmation");
        if(password.value.length < 8) {
          alert("Password length min 8");
          return false;
        }
        if(password.value != password_confirmation.value) {
          alert("Password and Password Confirmation don't match");
          return false;
        }
        if(privacy_policy.checked == false){
          alert("Must aggree with privacy policy and terms conditions");
          return false;
        }
        
      });
    });
    var modal = document.getElementById('id03');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>

</body>
</html>