<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>VALUCOIN THE GLOBAL CASHBACK TOKEN</title>
  <link href="imgs/logo.png" rel="shortcut icon" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="google-site-verification" content="jWPAxnvQuYQnpvyPol73km8nuGaoJl5E8wlHDY_bq10" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
  <script js.src= "https://connect.facebook.net/en_US/sdk.js"></script>
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <meta name="google-signin-client_id" content="139050077010-4lq9t4pur2fq7ivelnjqpu2k2kvt828m.apps.googleusercontent.com">
  <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/v1526884278587/recaptcha__id.js"></script>
  <script src="https://www.google.com/recaptcha/api.js"></script>
  <meta name="maValidation" content="47604a6984d19e08376e90ac487a8005">
  <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-113709182-2"></script>

<style media="screen">

.part{
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: -1;
}
}

</style>
</head>

<body style="background-color:#29af4b;">
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '220711231985693',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.0'
    });
    FB.getLoginStatus(function(response) {
      if (response.authResponse) {
            // Get and display the user profile data
      } else {
          
      }
    });
    FB.AppEvents.logPageView();
  };

  function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.first_name + '!';
        document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>Locale:</b> '+response.locale+'</p><p><b>Picture:</b> <img src="'+response.picture.data.url+'"/>';
    });
  }

  (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

    <div id="particles-js"></div>
    <div class="container" style=" height:99vh;">
@if($errors->has('email'))
  <div class="alert alert-success"><strong style="color:red">{{ $errors->first('email') }}</strong></div>
@endif
@if($message == "")
@elseif($message == "Register Success. Check your email to verify your account.")
  <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em>{{$message}}</em></div>
@elseif($message == "New Password Success")
  <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em>{{$message}}</em></div>
@else
  <div class="alert alert-success"><strong style="color:red">{{$message}}</strong></div>
@endif
      <div class="title">
        VALUCOIN
      </div>
      <div class="title-content">
        Discover valucoin and get the chance to redeem a total of 500.000 VALU reward (worth 70.000 USD) by following this easy step now!
      </div>
      <div class="master">

        <div class="row">
          <div class="col-lg-12">

                <img src="{{asset('imgs/all.png')}}" alt=""  usemap="#Map" style=""  class="img_three"/>
                  <map name="Map" id="Map">
                    <a  colspan="3" onclick="document.getElementById('id01').style.display='block'" style="cursor:pointer;" class="change-color">
                    <area alt="" title="" shape="poly" coords="15,13,300,13,299,154,273,162,261,173,253,188,247,204,248,216,250,229,255,236,15,387" /></a>
                    <area alt="" title="" onclick="checkLoginState()" shape="poly" coords="308,14,584,13,583,381,352,237,357,198,333,165,308,154" />
                    <a  colspan="3" onclick="document.getElementById('id02').style.display='block'" style="cursor:pointer;" class="change-color"><area alt="" title=""  shape="poly" coords="259,244,31,388,576,387,350,243,339,253,324,262,305,267,281,262" />
                    </a>
                  </map>


                <div class="button_three">
                  <div class="row">
                    <div class="col-xs-12">
                      <button type="button" onclick="checkLoginState()" name="button" class="FB_button">Login With Facebook</button>
                    </div>
                    <div class="col-xs-6">
                      <button type="button" onclick="document.getElementById('id01').style.display='block'" name="button" class="rl_button">Login</button>
                    </div>
                    <div class="col-xs-6">
                      <a href="/mregister"><button type="button"name="button" class="rl_button">Register</button></a>
                    </div>
                  </div>
                </div>
            <div class="col-lg-1" style=" ">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript" src="{{asset('js/particles.js')}}"></script>
<script type="text/javascript" src="{{asset('js/js/app.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(function(){
      $('form').submit(function(event) {
        var loginpassword = document.getElementById("login_password");
        if (loginpassword.value != '') {
          if(loginpassword.value.length < 8) {
            alert("Password Login length min 8");
            return false;
          }
        }
        else{
          var privacy_policy = document.getElementById("privacy_policy");
          console.log(privacy_policy.checked);
          var password = document.getElementById("password");
          var password_confirmation = document.getElementById("password_confirmation");
          if(password.value.length < 8) {
            alert("Password length min 8");
            return false;
          }
          if(password.value != password_confirmation.value) {
            alert("Password and Password Confirmation don't match");
            return false;
          }
          if(privacy_policy.checked == false){
            alert("Must aggree with privacy policy and terms conditions");
            return false;
          }
        }
      });
    });
  var modal = document.getElementById('id03');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
// Get the modal
var modal = document.getElementById('id01','id02');
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

<script>
  function checkLoginState() {
      FB.login(function(response) {
      getFbUserData();
    }, {scope: 'public_profile,email'});
  };

  function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        var id = response.id;
        var email = response.email;
        var fullname = response.first_name+" "+response.last_name;
        window.location = "{{URL('loginfacebook')}}/"+email+"&"+fullname+"&"+id;
    });
  };

  function onLoadGoogleCallback(){
    gapi.load('auth2', function() {
      auth2 = gapi.auth2.init({
        client_id: '139050077010-4lq9t4pur2fq7ivelnjqpu2k2kvt828m.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

    auth2.attachClickHandler(element, {},
      function(googleUser) {
          var profile = googleUser.getBasicProfile();
          var email = profile.getEmail();
          var fullname = profile.getName();
        }, 
      function(error) {
        
        }
      );
    });
    element = document.getElementById('googleSignIn');
  };

</script>
<script src="https://apis.google.com/js/platform.js?onload=onLoadGoogleCallback" async defer></script>

<!-- Modal Login -->
<div id="id01" class="modal" style="overflow:hidden;">
  <form class="modal-content animate" action="{{url('/login') }}" method="post">
       {{ csrf_field() }}
    <div class="imgcontainer">
      <!-- <B><center>Login Account</center></B>  -->
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="modal-container">
      <label for="email"><b>E-Mail</b></label><br>
      <input type="email" placeholder="Enter Email" name="login_email" id="login_email" required>
        <!-- @if ($errors->has('login_email'))
          <strong>{{ $errors->first('login_email') }}</strong>
        @endif -->
      <br>

      <label for="password"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="login_password" id="login_password" required>
      <a href="/forgot_password" class="fpw"><b>Forgot Password?</b></a>
        <!-- @if ($errors->has('login_password'))
          <strong>{{ $errors->first('login_password') }}</strong>
        @endif -->
      <button type="submit">Login</button>
  </div>
  </form>
</div>

<!-- Modal Register-->
<div id="id02" class="modal-register" style="overflow:hidden;">
  <form class="modal-content animate" method="POST" action="{{ url('/register') }}">
       {{ csrf_field() }}
    <div class="imgcontainer">
      <!-- <B><center>REGISTER</center></B> 
      <CENTER>Please Fill in This Form to Create an Account.</CENTER><br> -->
      <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="modal-container">
      <label for="name"><b>Username  </b></label>
      <input type="text" placeholder="Enter Username" name="name" id="name" required>
        <!-- @if ($errors->has('name'))
          <strong>{{ $errors->first('name') }}</strong>
        @endif -->
      <br>

      <label for="email"><b>E-Mail  </b></label><br>
      <input type="email" placeholder="Enter Email" name="email" id="email" required>
        <!-- @if ($errors->has('email'))
          <strong>{{ $errors->first('email') }}</strong>
        @endif -->
      <br>

      <label for="password"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="password" id="password" required>
        <!-- @if ($errors->has('password'))
          <strong>{{ $errors->first('password') }}</strong>
        @endif -->
        <br>
         <label for="password_confirmation"><b>Password Confirmation</b></label>
      <input type="password" placeholder="Enter Password Confirmation" name="password_confirmation" id="password_confirmation" required>
        <!-- @if ($errors->has('password_confirmation'))
          <strong>{{ $errors->first('password_confirmation') }}</strong>
        @endif -->
        <br>
      <input type="hidden" id="triggerhiddenregister" value="register"><br>
      <input type="checkbox" name="privacy_policy" id="privacy_policy" value="aggree"> I aggree with <a href="/privacy_policy" class="privacy-color">privacy policy</a> and <a href="/terms_conditions" class="privacy-color">terms conditions</a><br><br>
      <button type="submit" id="register_button">Register</button>
  </div>
  </form>
</div>

</body>
</html>
