<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>VALUCOIN THE GLOBAL CASHBACK TOKEN</title>
  <link href="imgs/logo.png" rel="shortcut icon" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="google-site-verification" content="jWPAxnvQuYQnpvyPol73km8nuGaoJl5E8wlHDY_bq10" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
  <script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/v1526884278587/recaptcha__id.js"></script>
  <script src="https://www.google.com/recaptcha/api.js"></script>
  <meta name="maValidation" content="47604a6984d19e08376e90ac487a8005">
  <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-113709182-2"></script>

<style media="screen">
.part{
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: -1;
}
</style>
</head>
<body style="background-color:#29af4b;">
  <div id="particles-js"></div>
  <div class="container" style=" height:99vh;">
    <div class="container-verif">
    <div class="title-verif">
      VALUCOIN
    </div>
    <div class="title-content"></div>
    <div class="master">
      <div class="row">
        <div class="col-lg-12">
            <div class="row">
              <div class="image-verif">
                <a href="{{URL('/')}}"><b><img height="100" src="{{URL::asset('imgs/logo.png')}}"/></b></a>
              </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="row">
              <div class="verif">
                <h3 style="color: rgb(41, 175, 75);">{{$message}}</h3>
              </div>
            </div>
        </div>
        <div class="col-lg-12">
          <a href="{{URL('/')}}"><button class="rl_button">Back to Home</button></a>
        </div>
       </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="{{asset('js/particles.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/js/app.js')}}"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
</body>
</html>