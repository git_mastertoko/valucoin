@extends('System.layout')
@section('content')
<div class="serv-price">
	<div class="container">

	<!-- Waves -->
	<form action="{{url('/buyvalu') }}" method="post">
    <div class="Merchant-big">
      <div class="Merchant">
        <h2 class="Merchantheader">Buy ValuCoin With Waves</h2>
      </div>
      <div class="row" id="">
        @foreach($pair as $pair)
        <div class="col-md-3">
          <div class="price-box">
            <div style="height:180px;">
              <img class="img-responsive center-block" src="imgs/logo.png" style="width:140px; padding-top: 20px; padding-bottom: 20px;" alt="Valu">
            </div>

            <form action="{{url('/transaction')}}" method="post">
              <h3 class="colorValu">Waves to ValuCoin</h3>
              Pairing Rate: <br>
               {{$pair->valucoin}} ValuCoin<br>
               {{$pair->waves}} Waves<br>
               {{$pair->valucoin_bonus}} Bonus ValuCoin
              @if(Auth::User()->user_waves_address == '')
              <a  colspan="3" onclick="document.getElementById('id04').style.display='block'" style="cursor:pointer;">
              <button type="button" class="price-button lgb" onclick="go_deps(1000)">Trade</button></a>
              @else
              <a colspan="3" id="{{$pair->pair_id}}" name="Waves" onclick="document.getElementById('id03').style.display='block'; document.getElementById('pair_id').value = this.id; document.getElementById('pair_coin_name').value = this.name; document.getElementById('p1').innerHTML = 'Are You Sure Want to Buy ValuCoin from Waves?'" style="cursor:pointer;"><button type="button" class="price-button lgb" onclick="go_deps(1000)">Buy</button></a>
              @endif
            </form>
          </div>
        </div>
 		@endforeach
      </div>
    </form>

    <!-- BTC -->
    <form action="{{url('/buyvalu') }}" method="post">
    <div class="Merchant-big">
      <div class="Merchant">
        <h2 class="Merchantheader">Buy ValuCoin With BTC</h2>
      </div>
      <div class="row" id="">
        @foreach($pair1 as $pair)
        <div class="col-md-3">
          <div class="price-box">
            <div style="height:180px;">
              <img class="img-responsive center-block" src="imgs/logo.png" style="width:140px; padding-top: 20px; padding-bottom: 20px;" alt="Valu">
            </div>

            <form action="{{url('/transaction')}}" method="post">
              <h3 class="colorValu">BTC to ValuCoin</h3>
              Pairing Rate: <br>
               {{$pair->valucoin}} ValuCoin<br>
               {{$pair->btc}} BTC<br>
               {{$pair->valucoin_bonus}} Bonus ValuCoin
              @if(Auth::User()->user_waves_address == '')
              <a  colspan="3" onclick="document.getElementById('id04').style.display='block'" style="cursor:pointer;">
              <button type="button" class="price-button lgb" onclick="go_deps(1000)">Trade</button></a>
              @else
              <a colspan="3" id="{{$pair->pair_id}}" name="btc" onclick="document.getElementById('id03').style.display='block'; document.getElementById('pair_id').value = this.id; document.getElementById('pair_coin_name').value = this.name; document.getElementById('p1').innerHTML = 'Are You Sure Want to Buy ValuCoin from BTC?'" style="cursor:pointer;"><button type="button" class="price-button lgb" onclick="go_deps(1000)">Buy</button></a>
              @endif
            </form>
          </div>
        </div>
 		@endforeach
      </div>
    </form>

    <!-- ETH -->
    <form action="{{url('/buyvalu') }}" method="post">
    <div class="Merchant-big">
      <div class="Merchant">
        <h2 class="Merchantheader">Buy ValuCoin With ETH</h2>
      </div>
      <div class="row" id="">
        @foreach($pair2 as $pair)
        <div class="col-md-3">
          <div class="price-box">
            <div style="height:180px;">
              <img class="img-responsive center-block" src="imgs/logo.png" style="width:140px; padding-top: 20px; padding-bottom: 20px;" alt="Valu">
            </div>

            <form action="{{url('/transaction')}}" method="post">
              <h3 class="colorValu">ETH to ValuCoin</h3>
              Pairing Rate: <br>
               {{$pair->valucoin}} ValuCoin<br>
               {{$pair->eth}} ETH<br>
               {{$pair->valucoin_bonus}} Bonus ValuCoin
              @if(Auth::User()->user_waves_address == '')
              <a  colspan="3" onclick="document.getElementById('id04').style.display='block'" style="cursor:pointer;">
              <button type="button" class="price-button lgb" onclick="go_deps(1000)">Trade</button></a>
              @else
              <a colspan="3" id="{{$pair->pair_id}}" name="eth" onclick="document.getElementById('id03').style.display='block'; document.getElementById('pair_id').value = this.id; document.getElementById('pair_coin_name').value = this.name; document.getElementById('p1').innerHTML = 'Are You Sure Want to Buy ValuCoin from ETH?'" style="cursor:pointer;"><button type="button" class="price-button lgb" onclick="go_deps(1000)">Buy</button></a>
              @endif
            </form>
          </div>
        </div>
 		@endforeach
      </div>
    </form>


<!-- Modal Popup For Valu Point to Valu Coin-->
  <div id="id04" class="modal" style="overflow:hidden;">
    <form method="POST" class="modal-content animate" action="{{ url('/address') }}">
      {{ csrf_field() }}
      <div class="imgcontainer">
        <span onclick="document.getElementById('id04').style.display='none'" class="close" title="Close Modal">&times;</span>
      </div>
      <div class="modal-container">
        <p id="pl_wait"><img src="imgs/logo.png" class="modalpict"><br>Input Your Address</p>
        <center> <input type="text" name="address" placeholder="Valu Coin Address"><br></center>
        <center><button type="submit" class="price-button lgb">Submit</button> </center>
      </div>
    </form>
  </div>
	</div>	
</div>

<div id="id03" class="modal" style="overflow:hidden;">
  <form class="modal-content animate" action="{{url('/buyvalu') }}" method="post">
    {{ csrf_field() }}
    <div class="imgcontainer">
      <span onclick="document.getElementById('id03').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <div class="modal-container">
      <input type="hidden" id="pair_id" name="pair_id"><br>
      <input type="hidden" id="pair_coin_name" name="pair_coin_name"><br>
      <label for="Sure"><b id="p1"></b></label>
    </div>
    <button type="submit" class="price-button lgb">Buy</button>
  </form>
</div>
@endsection