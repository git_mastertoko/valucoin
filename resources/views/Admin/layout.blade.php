<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VALUCOIN THE GLOBAL CASHBACK TOKEN</title>

    <!-- Bootstrap -->
    <link href="{{asset('cssadmin/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- NProgress -->
    <!-- CSS -->
    <link href="{{asset('cssadmin/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="{{asset('cssadmin/custom.css')}}" rel="stylesheet">

    <link href="{{asset('DataTables/datatables.cssadmin')}}" rel="stylesheet">
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/" class="site_title"></i> <span>ValuCoin</span></a>
            </div>
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>ValuCoin Admin</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br/>
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Airdrop <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/adminview')}}">Airdrop Admin View</a></li>
                       <li><a href="{{url('/')}}">Airdrop User View</a></li>
                    </ul>
                  </li>
                  <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i>Package Sell <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/adminpackage')}}">Package Admin View</a></li>
                       <li><a href="{{url('/home')}}">Package User View</a></li>
                    </ul>
                  </li>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings" href="/adminview">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true" onclick="toggleFullScreen()"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true" ></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <!-- <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class=""></li>
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green" style="top:1px !important"> </span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <center><a href="/dltntf" style="color: black;"><b><h5>Delete</h5></b></a></center>
                      </a>
                    </li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

    <script src="{{asset('jsadmin/jquery/jquery.js')}}"></script>
    <script src="{{asset('jsadmin/bootstrap/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('jsadmin/fastclick/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('jsadmin/nprogress/nprogress.js')}}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{asset('jsadmin/custom.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('DataTables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('custom/jquery.js')}}"></script>
    <script type="text/javascript">
        function toggleFullScreen() {
          if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
              (!document.mozFullScreen && !document.webkitIsFullScreen)) {
          if (document.documentElement.requestFullScreen) {  
              document.documentElement.requestFullScreen();  
          } else if (document.documentElement.mozRequestFullScreen) {  
              document.documentElement.mozRequestFullScreen();  
          } else if (document.documentElement.webkitRequestFullScreen) {  
              document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
          }  
          } else {  
          if (document.cancelFullScreen) {  
              document.cancelFullScreen();  
            } else if (document.mozCancelFullScreen) {  
              document.mozCancelFullScreen();  
            } else if (document.webkitCancelFullScreen) {  
              document.webkitCancelFullScreen();  
            }  
          }  
    }
</script>
        @section('content')
        @show
  </body>
</html>