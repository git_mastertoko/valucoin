@extends('Admin.layout')
@include('Admin.header')
@section('content')
  <!-- Info boxes -->

  <div class="container-check">	
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><img src="icon\trx.png" class="status-image"></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Transaction</span>
              <span class="info-box-number">{{$trx}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><img src="icon\member.png" class="status-image"></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Member</span>
              <span class="info-box-number">{{$user}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><img src="icon\sold.png" class="status-image"></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Valu Coin Sold </span>
              <span class="info-box-number">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><img src="icon\coin.png" class="status-image"></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Coin</span>
              <span class="info-box-number">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      {!!$content!!}


</div>
@endsection