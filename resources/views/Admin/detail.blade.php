@extends('Admin.layout')
@include('Admin.header')
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 80%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
@section('content')
<div class="container-check">
<table>
    <tr>
        <td><b>User Name & Email</b></td>
        <td>{{$user->user_account_name}}</td>
        <td>{{$user->user_email}}</td>

    </tr>
	<tr>
		<td><b>Waves Address</b></td>
		<td>{{$airdrop->wave_address}} </td>
		<td>
		@if($airdrop->wave_address_verify == 1)
    	<a href="{{url('verifyaddress')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-success edit_btn" id="$airdrop->airdrop_id">Verify</button></a>
    	<a href="{{url('rejectaddress')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-danger edit_btn" id="$airdrop->airdrop_id">Reject</button></a>
    	@elseif($airdrop->wave_address_verify == 2)
    	Verified
        <a href="{{url('undoaddress')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@elseif($airdrop->wave_address_verify == 3)
    	Rejected
        <a href="{{url('undoaddress')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
        @endif
        </td>
	</tr>
	<tr>
		<td><b>Facebook Profile Link</b></td>
		<td>{{$airdrop->facebook_profile_link}}</td>
		<td>
		@if($airdrop->facebook_profile_link_verify == 1)
    	<a href="{{url('verifylinkfb')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-success edit_btn" id="$airdrop->airdrop_id">Verify</button></a>
    	<a href="{{url('rejectlinkfb')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-danger edit_btn" id="$airdrop->airdrop_id">Reject</button></a>
    	@elseif($airdrop->facebook_profile_link_verify == 2)
    	Verified
        <a href="{{url('undolinkfb')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@elseif($airdrop->facebook_profile_link_verify == 3)
    	Rejected
        <a href="{{url('undolinkfb')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@endif
        </td>
	</tr>
	<tr>
		<td><b>Facebook Post Link</b></td>
		<td>{{$airdrop->facebook_post_link}}</td>
		<td>
		@if($airdrop->facebook_post_link_verify == 1)
    	<a href="{{url('verifypostfb')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-success edit_btn" id="$airdrop->airdrop_id">Verify</button></a>
    	<a href="{{url('rejectpostfb')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-danger edit_btn" id="$airdrop->airdrop_id">Reject</button></a>
    	@elseif($airdrop->facebook_post_link_verify == 2)
    	Verified
        <a href="{{url('undopostfb')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@elseif($airdrop->facebook_post_link_verify == 3)
    	Rejected
        <a href="{{url('undopostfb')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@endif
        </td>
	</tr>
	<tr>
		<td><b>Twitter Profile Link</b></td>
		<td>{{$airdrop->twitter_profile_link}}</td>
		<td>
		@if($airdrop->twitter_profile_link_verify == 1)
    	<a href="{{url('verifylinktweet')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-success edit_btn" id="$airdrop->airdrop_id">Verify</button></a>
    	<a href="{{url('rejectlinktweet')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-danger edit_btn" id="$airdrop->airdrop_id">Reject</button></a>
    	@elseif($airdrop->twitter_profile_link_verify == 2)
    	Verified
        <a href="{{url('undolinktweet')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@elseif($airdrop->twitter_profile_link_verify == 3)
    	Rejected
        <a href="{{url('undolinktweet')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@endif
        </td>
	</tr>
	<tr>
		<td><b>Twitter Retweet Link</b></td>
		<td>{{$airdrop->twitter_post_link}}</td>
		<td>
		@if($airdrop->twitter_post_link_verify == 1)
    	<a href="{{url('verifypostweet')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-success edit_btn" id="$airdrop->airdrop_id">Verify</button></a>
    	<a href="{{url('rejectpostweet')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-danger edit_btn" id="$airdrop->airdrop_id">Reject</button></a>
    	@elseif($airdrop->twitter_post_link_verify == 2)
    	Verified
        <a href="{{url('undopostweet')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@elseif($airdrop->twitter_post_link_verify == 3)
    	Rejected
        <a href="{{url('undopostweet')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@endif
        </td>
	</tr>
	<tr>
		<td><b>Telegram Account</b></td>
		<td>{{$airdrop->telegram_account}}</td>
		<td>
		@if($airdrop->telegram_account_verify == 1)
    	<a href="{{url('verifytelegram')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-success edit_btn" id="$airdrop->airdrop_id">Verify</button></a>
    	<a href="{{url('rejecttelegram')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-danger edit_btn" id="$airdrop->airdrop_id">Reject</button></a>
    	@elseif($airdrop->telegram_account_verify == 2)
    	Verified
        <a href="{{url('undotelegram')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@elseif($airdrop->telegram_account_verify == 3)
    	Rejected
        <a href="{{url('undotelegram')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@endif
        </td>
	</tr>
	<tr>
		<td><b>Youtube Account</b></td>
		<td>{{$airdrop->youtube_account}}</td>
		<td>
		@if($airdrop->youtube_account_verify == 1)
    	<a href="{{url('verifyyoutube')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-success edit_btn" id="$airdrop->airdrop_id">Verify</button></a>
    	<a href="{{url('rejectyoutube')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-danger edit_btn" id="$airdrop->airdrop_id">Reject</button></a>
    	@elseif($airdrop->youtube_account_verify == 2)
    	Verified
        <a href="{{url('undoyoutube')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@elseif($airdrop->youtube_account_verify == 3)
    	Rejected
        <a href="{{url('undoyoutube')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-warning edit_btn" id="$airdrop->airdrop_id">Undo</button></a>
    	@endif
        </td>
	</tr>
    <tr>
        <td><b>Total Coin</b></td>
        <td>{{$user->total_deposit_valu}}</td>
        <td>
            <a href="{{url('sendairdrop')}}/{{Crypt::encrypt($airdrop->airdrop_id)}}"><button class="btn btn-success edit_btn" id="$airdrop->airdrop_id">Sent Valu</button></a>
        </td>
    </tr>
</table>

</div>

@endsection