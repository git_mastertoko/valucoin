@extends('System.layout')
@section('content')

<div class="serv-price">
  <div class="container">
    <form action="{{url('/getWaves') }}" method="post">
    <div class="Merchant-big">
      <div class="Merchant">
        <h2 class="Merchantheader">Merchants</h2>
      </div>
      <div class="row" id="">
        @foreach($data1 as $data1)
        <div class="col-md-3">
          <div class="price-box">
            <div style="height:180px;">
              <img class="img-responsive center-block" src="imgs/logo.png" style="width:140px; padding-top: 20px; padding-bottom: 20px;" alt="Valu">
            </div>

            <form action="{{url('/transaction')}}" method="post">
              <h3 class="colorValu">Valu Coin to Valu Point</h3>
              Pairing Rate: <br>
              {{$data1->valu_coin}} Valu Coin<br>
              {{$data1->valu_point}} Valu Point<br>
              <input type="hidden" name="buy_valu_total" value="{{$data1->valu_coin}}">
              <input type="hidden" name="buy_point_total" value="{{$data1->valu_point}}">
              @if(Auth::User()->user_waves_address == '')
              <a  colspan="3" onclick="document.getElementById('id04').style.display='block'" style="cursor:pointer;">
              <button type="button" class="price-button lgb" onclick="go_deps(1000)">Trade</button></a>
              @else
              <a colspan="3" id="{{$data1->trade_id}}" onclick="document.getElementById('id03').style.display='block'; document.getElementById('trade_id').value = this.id; document.getElementById('p1').innerHTML = 'Are You Sure Want to Buy {{$data1->valu_point}} Valu Point?'" style="cursor:pointer;"><button type="button" class="price-button lgb" onclick="go_deps(1000)">Trade</button></a>
              @endif
            </form>
          </div>
        </div>
        @endforeach
      </div>
    </form>

<!-- Modal Popup For Valu Coin to Valu Point-->
<div id="id03" class="modal" style="overflow:hidden;">
  <form class="modal-content animate" action="{{url('/tradecointopoint') }}" method="post">
    {{ csrf_field() }}
    <div class="imgcontainer">
      <span onclick="document.getElementById('id03').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <div class="modal-container">
      <label for="pickmerchant">For Merchants</label>
      <select id="merchant_id" class="form-control" required name="merchant_id">
        <option value="">Choose..</option>
        @foreach($merchant as $temp)
        <option value="{{$temp->merchant_id}}">{{$temp->merchant_name}}</option>
        @endforeach
      </select>
      <input type="hidden" id="trade_id" name="trade_id"><br>
      <label for="Sure"><b id="p1"></b></label>
    </div>
    <button type="submit" class="price-button lgb">Trade</button>
  </form>
</div>

<!-- Modal Popup For Valu Point to Valu Coin-->
  <div id="id04" class="modal" style="overflow:hidden;">
    <form method="POST" class="modal-content animate" action="{{ url('/address') }}">
      {{ csrf_field() }}
      <div class="imgcontainer">
        <span onclick="document.getElementById('id04').style.display='none'" class="close" title="Close Modal">&times;</span>
      </div>
      <div class="modal-container">
        <p id="pl_wait"><img src="imgs/logo.png" class="modalpict"><br>Input Your Address</p>
        <center> <input type="text" name="address" placeholder="Valu Coin Address"><br></center>
        <center><button type="submit" class="price-button lgb">Submit</button> </center>
      </div>
    </form>
  </div>

        <div class="merchantbottom">
          <h2 class="Merchantheader2">Our Merchants</h2>
              <table id="customers">
                <tr>
                  <th>Merchant</th>
                  <th>Username</th>
                  <th>Points</th>
                  <th>Link / Unlink</th>
                </tr>
                @foreach($balanceMerchant as $balanceMerchant)
                  @if($balanceMerchant->merchant->role_name == 'merchant')
                  <tr>
                    <td class="tengah"><img src="{{$balanceMerchant->merchant->merchant_logo}}"></td>
                    <td>{{$balanceMerchant->merchant_link_unique_name}}</td>
                    <td>{{$balanceMerchant->balance_merchant}}</td>
                    @if($balanceMerchant->merchant_verify == 0)
                    <td><button id="{{Crypt::encrypt($balanceMerchant->merchant_id)}}" class="linkStyle1" type="button" onclick="document.getElementById('link').style.display='block'; document.getElementById('merchant_id_link').value = this.id">Link ID</button></td>
                    @elseif($balanceMerchant->merchant_verify == 1)
                    <td><button id="{{Crypt::encrypt($balanceMerchant->merchant_id)}}" class="linkStyle2" type="button" onclick="document.getElementById('unlink').style.display='block'; document.getElementById('merchant_id_unlink').value = this.id">Unlink ID</button></td>
                    @else
                    <td><button class="linkStyle3" type="button">Pending </button></td>
                    @endif
                  </tr>
                  @else
                  @endif
                @endforeach
              </table>
        </div>

<div id="link" class="modal" style="overflow:hidden;">
  <form class="modal-content animate" action="{{url('/linkmerchant') }}" method="post">
       {{ csrf_field() }}
    <div class="imgcontainer">
      <span onclick="document.getElementById('link').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <div class="modal-container">
      <label for="email"><b>Username</b></label>
      <input type="text" placeholder="Enter Username" name="username" required><br>
      <input type="hidden" id="merchant_id_link" name="merchant_id"><br>
      <button type="submit">Submit</button>
      <button type="button" onclick="document.getElementById('link').style.display='none'">Cancel</button>
  </div>
  </form>
</div>

<div id="unlink" class="modal" style="overflow:hidden;">
  <form class="modal-content animate" action="{{url('/unlinkmerchant') }}" method="post">
       {{ csrf_field() }}
    <div class="imgcontainer">
      <span onclick="document.getElementById('unlink').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
    <div class="modal-container">
      Are You Sure ?
      <input type="hidden" id="merchant_id_unlink" name="merchant_id"><br>
      <button type="submit">Submit</button>
      <button type="button" onclick="document.getElementById('unlink').style.display='none'">Cancel</button>
  </div>
  </form>
</div>

    <div class="Merchant">
      <div class="Merchant">
        <h2 class="Merchantheader">Customer</h3>
        </div>
        <div class="row">
          @foreach($data2 as $data2)
          <div class="col-md-3">
            <div class="price-box">
              <div style="height:180px;">
                <img class="img-responsive center-block" src="imgs/logo.png" style="width:140px; padding-top: 20px; padding-bottom: 20px;" alt="Valu">
              </div>
              <h3 class="colorValu">Valu Point to Valu Coin</h3>
              Pairing Rate: <br>
              {{$data2->valu_point}} Valu Point<br>
              {{$data2->valu_coin}} Valu Coin<br>
             <a  colspan="3" onclick="document.getElementById('id04').style.display='block'" style="cursor:pointer;">
              <button type="button" class="price-button lgb" onclick="go_deps(1000)">Trade</button></a>
            </div>
          </div>
          @endforeach
        </div>
       
        <div class="merchantbottom">
          <h2 class="Merchantheader2">Our Marketplaces</h2>
              <table id="customers">
                <tr>
                  <th>Merchant</th>
                  <th>Username</th>
                  <th>Points</th>
                  <th>Link / Unlink</th>
                </tr>
                @foreach($balanceUser as $balanceUser)
                  @if($balanceUser->merchant->role_name == 'user')
                  <tr>
                    <td class="tengah"><img src="{{$balanceUser->merchant->merchant_logo}}"></td>
                    <td>{{$balanceUser->merchant_link_unique_name}}</td>
                    <td>{{$balanceUser->balance_customer}}</td>
                    @if($balanceUser->merchant_verify == 0)
                    <td><button id="{{Crypt::encrypt($balanceUser->merchant_id)}}" class="linkStyle" type="button" onclick="document.getElementById('link').style.display='block'; document.getElementById('merchant_id_link').value = this.id">Link ID</button></td>
                    @else
                    <td><button id="{{Crypt::encrypt($balanceUser->merchant_id)}}" class="linkStyle" type="button" onclick="document.getElementById('unlink').style.display='block'; document.getElementById('merchant_id_unlink').value = this.id">Unlink ID</button></td>
                    @endif
                  </tr>
                  @else
                  @endif
                @endforeach
              </table>
        </div>
    </div>

    <div class="footer">
      <div class="container">
        © Valu Point 2018.<br>
      </div>

      <script>
// Get the modal
var modal = document.getElementById('id03','id04');
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
      @endsection
