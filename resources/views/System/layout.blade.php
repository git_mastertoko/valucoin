<!DOCTYPE html>
<html lang="en"><head>
<meta charset="utf-8">
<title>VALUCOIN THE GLOBAL CASHBACK TOKEN</title>
<link href="imgs/logo.png" rel="shortcut icon" type="image/x-icon">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="google-site-verification" content="jWPAxnvQuYQnpvyPol73km8nuGaoJl5E8wlHDY_bq10" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
<script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/api2/v1526884278587/recaptcha__id.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<meta name="maValidation" content="47604a6984d19e08376e90ac487a8005">
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-113709182-2"></script>
<script js.src= "https://connect.facebook.net/en_US/sdk.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<meta name="google-signin-client_id" content="139050077010-4lq9t4pur2fq7ivelnjqpu2k2kvt828m.apps.googleusercontent.com">
</head>
<body>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '220711231985693',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.getLoginStatus(function(response) {
      if (response.authResponse) {
            // Get and display the user profile data
      } else {
          
      }
    });
    FB.AppEvents.logPageView();
  };

  function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.first_name + '!';
        document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>Locale:</b> '+response.locale+'</p><p><b>Picture:</b> <img src="'+response.picture.data.url+'"/>';
    });
  }

  (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<div class="topmenu">
<div class="container">
<ul>
@if (Auth::guest())

<li><a  colspan="3" onclick="document.getElementById('id02').style.display='block'" style="cursor:pointer;" class="change-color">Register</a></li>
<li><a  colspan="3" onclick="document.getElementById('id01').style.display='block'" style="cursor:pointer;" class="change-color">Login</a></li>
<!-- <button id="googleSignIn" class="btn2 btn-Block btn-Google mar_bot" type="button">
  <i class="fa fa-google" aria-hidden="true">
    <span class="btn_tulisan" >Masuk Dengan Google</span>
  </i>
</button>
<div id="my-signin2"></div> -->
@else
<li><a class="change-color-1" href="/home" color="white";>Home</a></li>
<li><a class="change-color-1" href="/logout">Logout</a></li>
@endif
</ul>
<div class="clearfix"></div>
</div>
</div>

<div id="id01" class="modal" style="overflow:hidden;">
  <form class="modal-content animate" action="{{url('/login') }}" method="post">
  	   {{ csrf_field() }}
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="modal-container">
      <label for="email"><b>E-Mail</b></label><br>
      <input type="text" placeholder="Enter Email" name="email" required>
      	@if ($errors->has('email'))
       		<strong>{{ $errors->first('email') }}</strong>
        @endif
      <br>

      <label for="password"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="password" required>
      	@if ($errors->has('password'))
       		<strong>{{ $errors->first('password') }}</strong>
        @endif
      <button type="submit">Login</button>
      <button onclick="checkLoginState()" class="btn2 btn-Block btn-FB mar_bot" type="button">
        <i class="fa fa-facebook-official" aria-hidden="true">
          <span class="btn_tulisan" >Login with Facebook</span>
        </i>
      </button>
  </div>
  </form>
</div>


<div id="id02" class="modal" style="overflow:hidden;">
  <form class="modal-content animate" method="POST" action="{{ url('/register') }}">
  	   {{ csrf_field() }}
    <div class="imgcontainer">
      <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="modal-container">
      <label for="name"><b>Username  </b></label>
      <input type="text" placeholder="Enter Username" name="name" required>
      	@if ($errors->has('name'))
       		<strong>{{ $errors->first('name') }}</strong>
        @endif
      <br>

      <label for="email"><b>E-Mail  </b></label><br>
      <input type="text" placeholder="Enter Email" name="email" required>
      	@if ($errors->has('email'))
       		<strong>{{ $errors->first('email') }}</strong>
        @endif
      <br>

      <label for="password"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="password" required>
      	@if ($errors->has('password'))
       		<strong>{{ $errors->first('password') }}</strong>
        @endif
        <br>
         <label for="password_confirmation"><b>Password Confirmation</b></label>
      <input type="password" placeholder="Enter Password Confirmation" name="password_confirmation" required>
      	@if ($errors->has('password_confirmation'))
       		<strong>{{ $errors->first('password_confirmation') }}</strong>
        @endif
        <br>

      <button type="submit">Register</button>
  </div>
  </form>
</div>

        @section('content')

        @show

<script>
// Get the modal
var modal = document.getElementById('id01','id02');
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

<script>
  function checkLoginState() {
      FB.login(function(response) {
      getFbUserData();
    }, {scope: 'public_profile,email'});
  };

  function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        var id = response.id;
        var email = response.email;
        var fullname = response.first_name+" "+response.last_name;
    });
  };

  function onLoadGoogleCallback(){
    gapi.load('auth2', function() {
      auth2 = gapi.auth2.init({
        client_id: '139050077010-4lq9t4pur2fq7ivelnjqpu2k2kvt828m.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

    auth2.attachClickHandler(element, {},
      function(googleUser) {
          var profile = googleUser.getBasicProfile();
          var email = profile.getEmail();
          var fullname = profile.getName();
        }, 
      function(error) {
        
        }
      );
    });
    element = document.getElementById('googleSignIn');
  };

</script>
<script src="https://apis.google.com/js/platform.js?onload=onLoadGoogleCallback" async defer></script>


  </body>
</html>
