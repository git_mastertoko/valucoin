<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VALUCOIN THE GLOBAL CASHBACK TOKEN</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="icon" href="https://token.valucoin.co/imgs/logo.png">
</head>
<body  style="margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: Arial, Verdana, sans-serif;">
<table width="100%" height="100%" style="padding: 20px 0px 20px 0px" bgcolor="#ececec" >
  <tr  align="center">
    <td>
    <!--Start Header-->
    <table width="562" cellpadding="0" cellspacing="0" style="color:#202020; font-weight: bold; padding: 16px 0px 16px 14px; font-family: Arial, Verdana, sans-serif; background-color: #202020">
      <tr>
        <td>
          <img href="https://token.valucoin.co/imgs/logo.png" height="100" width="100" style="" >
          <span style="font-size: 35px;">Valucoin</span>
        </td>
      </tr>
    </table>
    <!--end header-->


<!--Start Ribbon-->
      <table cellpadding="0" cellspacing="0" width="562" bgcolor="#202020">
        <tr>
          <td width="300" height="203" bgcolor="#202020" style="font-family: arial, verdana, sans-serif; padding: 10px 25px 0px 15px; font-size: 12px; color:#FFFFFF; ">
          <span style="text-transform: uppercase;font-size: 30px; font-weight: bold; color: #FFFFFF;"> Valucoin</span><br><br>
          <span style="width: 525px; color: #FFFFFF;">Verify Your Valucoin Account by </span>
          <a style="font-weight: 700; font-size: 18px; color: #6aead2;" href="{{ $path }}" >clicking here</a>.
          <br><br>
          <span style="width: 525px; color: #FFFFFF;">or copy this link : {{ $path }} </span>
          <br><br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>