<?php

use Illuminate\Database\Seeder;

class DetailBalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detail_balance')->insert([
            'user_id'					=> '1',
            'merchant_id'				=> '1',
            'merchant_link_unique_name' => 'merchant@blackstoneindonesia.com',
            'balance_merchant'			=> 3000,
            'balance_customer'			=> 0,
            'created_at'  				=> '2018-06-09 00:00:00',
            'merchant_verify'           => 1
        ]);

        DB::table('detail_balance')->insert([
            'user_id'					=> '1',
            'merchant_id'				=> '2',
            'merchant_link_unique_name' => 'merchant@kartumi.com',
            'balance_merchant'			=> 2000,
            'balance_customer'			=> 0,
            'created_at'  				=> '2018-06-09 00:00:00',
            'merchant_verify'           => 0
        ]);

        DB::table('detail_balance')->insert([
            'user_id'					=> '1',
            'merchant_id'				=> '3',
            'merchant_link_unique_name' => 'merchant@hellobill.id',
            'balance_merchant'			=> 5000,
            'balance_customer'			=> 0,
            'created_at'  				=> '2018-06-09 00:00:00',
            'merchant_verify'           => 1
        ]);

        DB::table('detail_balance')->insert([
            'user_id'					=> '1',
            'merchant_id'				=> '4',
            'merchant_link_unique_name' => 'merchant@ezservices.co.id',
            'balance_merchant'			=> 7000,
            'balance_customer'			=> 0,
            'created_at'  				=> '2018-06-09 00:00:00',
            'merchant_verify'           => 0
        ]);

        DB::table('detail_balance')->insert([
            'user_id'					=> '1',
            'merchant_id'				=> '5',
            'merchant_link_unique_name' => 'merchant@kc-englishspeakup.com',
            'balance_merchant'			=> 9000,
            'balance_customer'			=> 0,
            'created_at'  				=> '2018-06-09 00:00:00',
            'merchant_verify'           => 1
        ]);

        DB::table('detail_balance')->insert([
            'user_id'                   => '1',
            'merchant_id'               => '6',
            'merchant_link_unique_name' => 'merchant@riptafood.com',
            'balance_merchant'          => 3000,
            'balance_customer'          => 0,
            'created_at'                => '2018-06-09 00:00:00',
            'merchant_verify'           => 0
        ]);

        DB::table('detail_balance')->insert([
            'user_id'                   => '1',
            'merchant_id'               => '7',
            'merchant_link_unique_name' => 'user@kartumi.com',
            'balance_merchant'          => 0,
            'balance_customer'          => 9000,
            'created_at'                => '2018-06-09 00:00:00',
            'merchant_verify'           => 1
        ]);

        DB::table('detail_balance')->insert([
            'user_id'                   => '1',
            'merchant_id'               => '8',
            'merchant_link_unique_name' => 'user@blackstone.com',
            'balance_merchant'          => 0,
            'balance_customer'          => 3000,
            'created_at'                => '2018-06-09 00:00:00',
            'merchant_verify'           => 0
        ]);
    }
}
