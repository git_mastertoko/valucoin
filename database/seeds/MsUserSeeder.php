<?php

use Illuminate\Database\Seeder;

class MsUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('msuser')->insert([
            'user_account_name'				=> 'Valupoint',
            'user_password'					=> bcrypt('valupoint'),
            'user_email'					=> 'valupoint@valucoin.co',
            'user_total_balance_merchant'	=> 0,
            'user_total_balance_customer'	=> 0,
            'user_verif'                    => 1,
            'referral_link_generate'        => 'https://token.valucoin.co/ref/MQ==',
            'referral_link_receive'         => '',
            'user_waves_address'			=> '3P5Yt3aYH8bbCS1Y8YQoa9kn2iaxqiKMxoD',
            'created_by'					=> 'Valupoint',
            'created_at'  					=> '2018-06-09 00:00:00'
        ]);
    }
}
