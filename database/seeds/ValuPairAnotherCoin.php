<?php

use Illuminate\Database\Seeder;

class ValuPairAnotherCoin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msvalupair')->insert([
        	'valucoin'		=> '100',
        	'valucoin_bonus'=> '5',
        	'waves'			=> '20',
        	'btc'			=> '0.003',
        	'eth'			=> '0.01',
        ]);

         DB::table('msvalupair')->insert([
        	'valucoin'		=> '1000',
        	'valucoin_bonus'=> '100',
        	'waves'			=> '200',
        	'btc'			=> '0.03',
        	'eth'			=> '0.1',
        ]);

         DB::table('msvalupair')->insert([
        	'valucoin'		=> '10000',
        	'valucoin_bonus'=> '1500',
        	'waves'			=> '2000',
        	'btc'			=> '0.3',
        	'eth'			=> '1',
        ]);

         DB::table('msvalupair')->insert([
        	'valucoin'		=> '100000',
        	'valucoin_bonus'=> '20000',
        	'waves'			=> '20000',
        	'btc'			=> '3',
        	'eth'			=> '10',
        ]);
    }
}
