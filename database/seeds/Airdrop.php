<?php

use Illuminate\Database\Seeder;

class Airdrop extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('msairdrop')->insert([
            'user_id'                     => 1,
            'email'					      => 'valupoint@valucoin.co',
            'wave_address'				  => '3P5Yt3aYH8bbCS1Y8YQoa9kn2iaxqiKMxoD',
            'facebook_profile_link'		  => 'https://www.facebook.com/valucoin/',
            'facebook_post_link'		  => 'https://www.facebook.com/valucoin/',
            'twitter_profile_link'        => 'https://twitter.com/valucoin',
            'twitter_post_link'           => 'https://twitter.com/valucoin',
            'telegram_account'            => 'Valucoin',
            'youtube_account'             => 'ValuCoin',
            'date'      				  => '2018-06-29',
            'email_verify'				  => 2,
            'wave_address_verify'	      => 2,
            'facebook_profile_link_verify'=> 1,
            'facebook_post_link_verify'	  => 1,
            'twitter_profile_link_verify' => 1,
            'twitter_post_link_verify'    => 1,
            'telegram_account_verify'     => 1,
            'youtube_account_verify'      => 1
        ]);
    }
}
