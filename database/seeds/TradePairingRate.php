<?php

use Illuminate\Database\Seeder;

class TradePairingRate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mstrade')->insert([
        	'trade_status'		=> 1,
        	'valu_coin'			=> 10,
        	'valu_point'		=> 100,
        ]);

        DB::table('mstrade')->insert([
        	'trade_status'		=> 1,
        	'valu_coin'			=> 100,
        	'valu_point'		=> 1000,
        ]);

        DB::table('mstrade')->insert([
        	'trade_status'		=> 1,
        	'valu_coin'			=> 1000,
        	'valu_point'		=> 10000,
        ]);

        DB::table('mstrade')->insert([
        	'trade_status'		=> 1,
        	'valu_coin'			=> 10000,
        	'valu_point'		=> 100000,
        ]);

        DB::table('mstrade')->insert([
        	'trade_status'		=> 2,
        	'valu_coin'			=> 10,
        	'valu_point'		=> 100,
        ]);

        DB::table('mstrade')->insert([
        	'trade_status'		=> 2,
        	'valu_coin'			=> 100,
        	'valu_point'		=> 1000,
        ]);

        DB::table('mstrade')->insert([
        	'trade_status'		=> 2,
        	'valu_coin'			=> 1000,
        	'valu_point'		=> 10000,
        ]);

        DB::table('mstrade')->insert([
        	'trade_status'		=> 2,
        	'valu_coin'			=> 10000,
        	'valu_point'		=> 100000,
        ]);
    }
}
