<?php

use Illuminate\Database\Seeder;

class MsMerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msmerchant')->insert([
            'merchant_name'		=> 'Blackstone',
            'role_name'			=> 'merchant',
            'created_at'  		=> '2018-06-09 00:00:00',
            'merchant_logo'     => 'imgs/blackstone.png',
            'merchant_url'      => 'http://blackstoneindonesia.com'
        ]);

        DB::table('msmerchant')->insert([
            'merchant_name'		=> 'Kartumi',
            'role_name'			=> 'merchant',
            'created_at'  		=> '2018-06-09 00:00:00',
            'merchant_logo'     => 'imgs/Kartumi.png',
            'merchant_url'      => 'https://kartumi.com'
        ]);

        DB::table('msmerchant')->insert([
            'merchant_name'		=> 'Hello Bill',
            'role_name'			=> 'merchant',
            'created_at'  		=> '2018-06-09 00:00:00',
            'merchant_logo'     => 'imgs/HelloBill.png',
            'merchant_url'      => 'https://hellobill.id'
        ]);

        DB::table('msmerchant')->insert([
            'merchant_name'     => 'EZ Parking',
            'role_name'         => 'merchant',
            'created_at'        => '2018-06-09 00:00:00',
            'merchant_logo'     => 'imgs/Ezparking.png',
            'merchant_url'      => 'http://www.ezservices.co.id'
        ]);

        DB::table('msmerchant')->insert([
            'merchant_name'     => 'KC English',
            'role_name'         => 'merchant',
            'created_at'        => '2018-06-09 00:00:00',
            'merchant_logo'     => 'imgs/KC-English.png',
            'merchant_url'      => 'http://kc-englishspeakup.com'
        ]);

        DB::table('msmerchant')->insert([
            'merchant_name'     => 'Ripta Food',
            'role_name'         => 'merchant',
            'created_at'        => '2018-06-09 00:00:00',
            'merchant_logo'     => 'imgs/riptafood.png',
            'merchant_url'      => 'http://riptafood.com'
        ]);
        DB::table('msmerchant')->insert([
            'merchant_name'		=> 'Kartumi User',
            'role_name'			=> 'user',
            'created_at'  		=> '2018-06-09 00:00:00',
            'merchant_logo'     => 'imgs/Kartumi.png',
            'merchant_url'      => 'https://kartumi.com'
        ]);

        DB::table('msmerchant')->insert([
            'merchant_name'     => 'Blackstone User',
            'role_name'         => 'user',
            'created_at'        => '2018-06-09 00:00:00',
            'merchant_logo'     => 'imgs/blackstone.png',
            'merchant_url'      => 'http://blackstoneindonesia.com'
        ]);
    }
}
