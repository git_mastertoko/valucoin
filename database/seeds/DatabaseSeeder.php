<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MsUserSeeder::class);
        $this->call(MsMerchantSeeder::class);
        $this->call(DetailBalanceSeeder::class);
        $this->call(TradePairingRate::class);
        $this->call(Airdrop::class);
        $this->call(ValuPairAnotherCoin::class);
        // $this->call(packagetest::class);
    }
}
