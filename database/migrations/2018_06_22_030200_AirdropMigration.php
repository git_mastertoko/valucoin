<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AirdropMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msairdrop',function(Blueprint $table){
            $table->increments('airdrop_id');
            $table->integer('user_id')->unsigned();
            $table->string('email');
            $table->string('wave_address');
            $table->string('facebook_profile_link');
            $table->string('facebook_post_link');
            $table->date('date');
            $table->string('twitter_profile_link');
            $table->string('twitter_post_link');
            $table->string('telegram_account');
            $table->string('youtube_account');
            $table->tinyInteger('email_verify');
            $table->tinyInteger('wave_address_verify');
            $table->tinyInteger('facebook_profile_link_verify');
            $table->tinyInteger('facebook_post_link_verify');
            $table->tinyInteger('twitter_profile_link_verify');
            $table->tinyInteger('twitter_post_link_verify');
            $table->tinyInteger('telegram_account_verify');
            $table->tinyInteger('youtube_account_verify');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('user_id')->on('msuser');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
