<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ValuCoinPairMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msvalupair',function(Blueprint $table){
          $table->increments('pair_id');
          $table->integer('valucoin');
          $table->integer('valucoin_bonus');
          $table->double('waves')->default('0');
          $table->double('btc')->default('0');
          $table->double('eth')->default('0');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
