<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MsMerchantMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msmerchant',function(Blueprint $table){
            $table->increments('merchant_id');
            $table->string('merchant_name');
            $table->string('merchant_logo')->default('');
            $table->string('merchant_url')->default('');
            $table->string('role_name');
            $table->timestamps(); 
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
