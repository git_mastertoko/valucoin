<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trs_buy_point',function(Blueprint $table){
            $table->increments('buy_point_id');
            $table->integer('user_id')->unsigned();
            $table->integer('merchant_id')->unsigned();
            $table->integer('sell_valu_total');
            $table->integer('buy_point_total');
            $table->string('buy_point_code');
            $table->string('txid')->default('');
            $table->timestamps(); 
            $table->softDeletes();
            $table->foreign('user_id')->references('user_id')->on('msuser');
            $table->foreign('merchant_id')->references('merchant_id')->on('msmerchant');
        });

        Schema::create('trs_sell_point',function(Blueprint $table){
            $table->increments('sell_point_id');
            $table->integer('user_id')->unsigned();
            $table->integer('merchant_id')->unsigned();
            $table->integer('buy_valu_total');
            $table->integer('sell_point_total');
            $table->string('sell_point_code');
            $table->string('txid')->default('');
            $table->timestamps(); 
            $table->softDeletes();
            $table->foreign('user_id')->references('user_id')->on('msuser');
            $table->foreign('merchant_id')->references('merchant_id')->on('msmerchant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
