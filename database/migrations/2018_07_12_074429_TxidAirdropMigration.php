<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TxidAirdropMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('msairdrop',function(Blueprint $table){
            $table->string('tx_id_airdrop')->after('youtube_account_verify')->default('');
            $table->string('tx_code_airdrop')->after('tx_id_airdrop')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
