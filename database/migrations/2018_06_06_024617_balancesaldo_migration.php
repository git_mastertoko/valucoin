<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BalancesaldoMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_balance',function(Blueprint $table){
            $table->increments('balance_id');
            $table->integer('user_id')->unsigned();
            $table->integer('merchant_id')->unsigned();
            $table->tinyInteger('merchant_verify');
            $table->string('merchant_link_unique_name')->default('');
            $table->bigInteger('balance_merchant');
            $table->bigInteger('balance_customer');
            $table->timestamps(); 
            $table->softDeletes();
            $table->foreign('user_id')->references('user_id')->on('msuser');
            $table->foreign('merchant_id')->references('merchant_id')->on('msmerchant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
