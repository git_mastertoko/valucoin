<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForgotPassword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('forgot_password',function(Blueprint $table){
            $table->increments('forgot_password_id');
            $table->integer('user_id')->unsigned();
            $table->string('email');
            $table->string('token');
            $table->timestamps(); 
            $table->softDeletes();
            $table->foreign('user_id')->references('user_id')->on('msuser');
        });

        Schema::table('msuser', function (Blueprint $table) {
            $table->integer('total_deposit_valu')->after('user_verif')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
