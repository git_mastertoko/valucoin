<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuyValuCoinMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trs_buy_valucoin',function(Blueprint $table){
            $table->increments('buy_valucoin_id');
            $table->integer('user_id')->unsigned();
            $table->integer('valu_coin');
            $table->integer('valu_bonus');
            $table->string('pair_coin_name');
            $table->double('pair_coin');
            $table->string('tx_code');
            $table->string('txid')->default('');
            $table->timestamps(); 
            $table->softDeletes();
            $table->foreign('user_id')->references('user_id')->on('msuser');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
