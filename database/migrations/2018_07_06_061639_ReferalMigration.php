<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReferalMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_bonus',function(Blueprint $table){
            $table->increments('referral_id');
            $table->integer('user_id')->unsigned();
            $table->integer('buy_valucoin_id');
            $table->string('referral_link');
            $table->integer('user_referral_id');
            $table->integer('total_buy');
            $table->integer('total_bonus');
            $table->timestamps(); 
            $table->softDeletes();
            $table->foreign('user_id')->references('user_id')->on('msuser');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
