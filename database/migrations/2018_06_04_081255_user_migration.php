<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msuser',function(Blueprint $table){
            $table->increments('user_id');
            $table->string('user_account_name');
            $table->string('user_password');
            $table->string('user_email');
            $table->bigInteger('user_total_balance_merchant');
            $table->bigInteger('user_total_balance_customer');
            $table->string('user_waves_address');
            $table->tinyInteger('user_verif')->default(0);
            $table->string('created_by');
            $table->string('edited_by')->nullable();
            $table->string('referral_link_generate');
            $table->string('referral_link_receive');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
